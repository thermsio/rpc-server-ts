import { RPCServer } from '../index'
import { CallResponse } from '../CallResponse'

const rpcGatewayServer = new RPCServer({
  displayName: 'example-gateway',
  ephemeralId: Math.random().toString(),
  server: {
    http: {
      port: 3000,
      serveDocs: true,
    },
    websocket: {
      onClientConnect: async (connection) => {
        console.log('EXMPL#onClientConnect:', connection)
      },
      onClientConnectionIdentityChanged: async (connection) => {
        console.log('EXMPL#onClientConnectionIdentityChanged:', connection)
      },
      onClientDisconnect: async (connection) => {
        console.log('EXMPL#onClientDisconnect:', connection)
      },
      port: 3001,
    },
  },
  // messageBroker: {
  // amqpURI: 'amqp://localhost:5672',
  // },
})

rpcGatewayServer.registerHandler(
  { procedure: 'login', scope: 'auth', version: '1' },
  async (request) => {
    console.log('EXMPL#login:', request)
    return new CallResponse(
      {
        code: 200,
        data: {
          id: (Math.random() * 100000).toString(),
          email: 'cory@therms.io',
          user: { name: 'Cory Robinson' },
        },
        success: true,
      },
      request,
    )
  },
)

rpcGatewayServer.start()
