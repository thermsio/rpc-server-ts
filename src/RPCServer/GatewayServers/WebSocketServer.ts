import WsServer from 'ws'
import WebSocket from 'ws'
import { DEFAULT_BIND, Server } from './Server'
import { Handler } from '../../Handler'
import { RPCServerConfig } from '../RPCServer'
import Debug from 'debug'
import { CallRequest, ICallRequestDTO } from '../../CallRequest'
import { CallResponse } from '../../CallResponse'
import { isIdentityValid, RPCClientIdentity } from '../../RPCClientIdentity'
import { IncomingMessage } from 'http'
import uuid from 'node-uuid'
import { isEqual } from 'lodash'

const debug = Debug('rpc:WebSocketServer')
// const debugMessages = Debug('rpc:WebSocketServer:messages')

export type TClientMessageHandler = (
  clientMsgHandler: IWebSocketClientMessage & { connectionId: string },
) => void

interface IWebSocketClientMessage {
  clientMessage: any
  identity?: RPCClientIdentity
}

interface WebSocketServerOptions {
  config: RPCServerConfig
}

export class WebSocketServer implements Server {
  private host: string
  private clientMessageHandlers: TClientMessageHandler[] = []
  private connectionsByConnectionId = new Map<string, WebSocket>()
  private port: number
  private requestHandler?: Handler
  private wss?: WsServer.Server
  // a local flag to track if the server is shutting down gracefully
  private wssShuttingDown = false

  constructor(readonly options: WebSocketServerOptions) {
    if (!options.config.server?.websocket?.port) {
      throw new Error('config.gatewayServer.websocket.port is required')
    }

    this.host = options.config.server?.websocket?.host || DEFAULT_BIND
    this.port = options.config.server?.websocket?.port

    if (options.config.server.websocket.onClientMessage) {
      this.clientMessageHandlers.push(
        options.config.server.websocket.onClientMessage,
      )
    }
  }

  name = 'WebSocketServer'

  private static parseIPAddressFromWsReq(req: IncomingMessage): string {
    let ip = ''

    if (req.headers['x-real-ip']) {
      if (Array.isArray(req.headers['x-real-ip'])) {
        ip = req.headers['x-real-ip'][0]
      } else {
        ip = req.headers['x-real-ip'].split(',')[0]
      }
    } else {
      ip = req.socket.remoteAddress || ''
    }

    ip = ip.trim()

    return ip
  }

  /**
   * This method allows adding a callback for websocket client messages to be handled that are not RPC requests.
   * @param handler
   */
  addClientMessageHandler = (handler: TClientMessageHandler) => {
    this.clientMessageHandlers.push(handler)
  }

  removeClientMessageHandler = (handler: TClientMessageHandler) => {
    const indexToRemove = this.clientMessageHandlers.findIndex(
      (func) => func === handler,
    )

    if (indexToRemove > -1) {
      this.clientMessageHandlers.splice(indexToRemove, 1)
    }
  }

  setIncomingRequestHandler = (requestHandler: Handler) => {
    this.requestHandler = requestHandler
  }

  sendMessageToClient = (connectionId: string, data: any) => {
    const ws = this.connectionsByConnectionId.get(connectionId)

    if (ws) {
      if (ws.readyState !== WebSocket.OPEN) {
        debug(
          'WebSocketServer.sendMessageToClient() removing connectionId - ws.readyState is not OPEN:',
          connectionId,
          ws.readyState,
        )

        this.connectionsByConnectionId.delete(connectionId)
        ws.terminate()

        return
      }

      let serverMessage

      try {
        serverMessage = JSON.stringify({ serverMessage: data })
      } catch (e) {
        console.log(
          'WebSocketServer.sendMessageToClient() error stringify serverMessage to send to client: ',
          serverMessage,
        )
      }

      if (serverMessage) {
        ws.send(serverMessage)
      }
    } else {
      console.warn(
        'WebSocketServer.sendMessageToClient() removing connectionId - no ws found for connectionId:',
        connectionId,
      )

      this.connectionsByConnectionId.delete(connectionId)

      if (this.options.config?.server?.websocket?.onClientDisconnect) {
        this.options.config.server.websocket.onClientDisconnect({
          connectionId,
        })
      }
    }
  }

  start = (onStartedCallback?: () => void) => {
    this.wssShuttingDown = false
    this.connectionsByConnectionId.clear()

    if (!this.requestHandler) {
      throw new Error(`No request handler has be set`)
    }

    this.wss = new WsServer.Server({
      host: this.host,
      port: this.port,
    })

    this.wss.on('listening', () => {
      console.info(`RPC WebSocketServer listening ${this.host}:${this.port}`)
    })

    this.wss.on('error', (err) => {
      console.error(`RPC WebSocketServer error - stopping ws server...`, err)
      this.wss?.close()
    })

    this.wss.on('close', () => {
      this.connectionsByConnectionId.clear()

      if (!this.wssShuttingDown) {
        console.error(
          `RPC WebSocketServer closed unexpectedly - restarting ws server...`,
        )
        this.wss = undefined
        setTimeout(() => {
          this.start()
        }, 1000) // Reconnect after a delay
      }
    })

    this.wss.on('connection', (ws: WebSocket, req) => {
      const websocketId = uuid.v4()

      this.connectionsByConnectionId.set(websocketId, ws)

      let identity: RPCClientIdentity | undefined

      const wsIp = WebSocketServer.parseIPAddressFromWsReq(req)

      debug('websocket connected, connectionId:', websocketId, ' ip:', wsIp)

      if (this.options.config?.server?.websocket?.onClientConnect) {
        this.options.config.server.websocket.onClientConnect({
          connectionId: websocketId,
          identity,
          ip: wsIp,
        })
      }

      ws.on('close', (code, data) => {
        this.connectionsByConnectionId.delete(websocketId)

        const reason = code || data.toString()

        debug('websocket disconnected, connectionId:', websocketId, reason)

        if (this.options.config?.server?.websocket?.onClientDisconnect) {
          this.options.config.server.websocket.onClientDisconnect({
            connectionId: websocketId,
            identity,
            ip: wsIp,
          })
        }
      })

      // when there is a client connection we need to handle it so it's not fatal
      ws.on('error', (err) => {
        console.error(`RPC WebSocketServer connection error, ${wsIp}`, err)
        this.connectionsByConnectionId.delete(websocketId)
        ws.close()
      })

      ws.on('message', async (data: WsServer.Data) => {
        const message = data?.toString()

        // debugMessages(
        //   'received message from WS, connectionId:',
        //   websocketId,
        //   message,
        // )

        let messageParsed

        if (typeof message === 'string') {
          try {
            messageParsed = JSON.parse(message)

            if (
              typeof messageParsed !== 'object' ||
              Array.isArray(messageParsed)
            ) {
              // debugMessages(
              //   'WebSocketServer error: received unparseable WS msg object',
              //   message,
              // )

              return
            }
          } catch (e: any) {
            // debugMessages(
            //   'WebSocketServer error: received unparseable WS msg string',
            //   message,
            // )

            return
          }
        } else {
          // debugMessages(
          //   'WebSocketServer error: received unknown "message" from client',
          //   message,
          // )

          return
        }

        const incomingMessage: ICallRequestDTO | IWebSocketClientMessage =
          messageParsed

        if (incomingMessage.identity) {
          if (!isIdentityValid(incomingMessage.identity)) {
            console.error(
              'error receiving CallResponseDTO with invalid identity schema',
              incomingMessage.identity,
            )

            ws.send(
              JSON.stringify(
                CallResponse.toCallResponseDTO(
                  new CallResponse(
                    {
                      code: 400,
                      message: 'invalid identity schema - disconnecting',
                      success: false,
                    },
                    CallRequest.fromCallRequestDTO(
                      {
                        ...incomingMessage,
                        // we add a procedure because CallRequest.fromCallRequestDTO requires a procedure on incoming RPC's
                        procedure: '__internal-identity__',
                      },
                      { trace: { caller: 'WebSocketServer' } },
                    ),
                  ),
                ),
              ),
              () => ws.close(),
            )

            return
          }
          // else {
          //   debugMessages('received identity:', incomingMessage.identity)
          // }

          if (
            identity?.authorization &&
            identity.authorization !== incomingMessage.identity.authorization
          ) {
            console.error(
              'error, a websocket changed identity.authorization from previous value, disconnecting...',
            )

            ws.send(
              JSON.stringify(
                CallResponse.toCallResponseDTO(
                  new CallResponse(
                    {
                      code: 400,
                      message:
                        'identity.authorization has changed - disconnecting websocket connection',
                      success: false,
                    },
                    CallRequest.fromCallRequestDTO(
                      {
                        ...incomingMessage,
                        // we add a procedure because CallRequest.fromCallRequestDTO requires a procedure on incoming RPC's
                        procedure: '__internal-identity__',
                      },
                      { trace: { caller: 'WebSocketServer' } },
                    ),
                  ),
                ),
              ),
              () => ws.close(),
            )

            return
          }

          if (incomingMessage.identity) {
            if (
              this.options.config?.server?.websocket
                ?.onClientConnectionIdentityChanged
            ) {
              if (!isEqual(incomingMessage.identity, identity)) {
                this.options.config.server.websocket.onClientConnectionIdentityChanged(
                  {
                    connectionId: websocketId,
                    identity: incomingMessage.identity,
                    ip: wsIp,
                  },
                )
              }
            }
          }

          identity = incomingMessage.identity
        }

        if ('clientMessage' in incomingMessage) {
          this.clientMessageHandlers.forEach((msgHandler) => {
            try {
              msgHandler({
                ...incomingMessage,
                connectionId: websocketId,
                identity,
              })
            } catch (err) {
              console.log(
                'error calling clientMessageHandler() with incoming websocket client msg',
                err,
              )
            }
          })

          return
        }

        const requestDTO: ICallRequestDTO = incomingMessage

        // This is just a "set my identity" msg from the client
        if (!requestDTO.procedure && requestDTO.identity) {
          try {
            const response = new CallResponse(
              {
                code: 200,
                correlationId: requestDTO.correlationId,
                success: true,
              },
              CallRequest.fromCallRequestDTO(
                {
                  ...requestDTO,
                  // we add a procedure because CallRequest.fromCallRequestDTO requires a procedure on incoming RPC's
                  procedure: '__internal-set-identity__',
                },
                { trace: { caller: 'WebSocketServer' } },
              ),
            )

            ws.send(JSON.stringify(CallResponse.toCallResponseDTO(response)))
          } catch (e: any) {
            console.log(
              'error sending CallResponseDTO to WS connection after setting RPCClientIdentity, error:',
              e,
            )
          }

          return
        }

        let request: CallRequest | undefined

        try {
          request = CallRequest.fromCallRequestDTO(
            { identity, ...requestDTO },
            { trace: { caller: 'WebSocketServer', ipAddress: wsIp } },
          )
        } catch (e: any) {
          // debugMessages(
          //   'WebSocketServer error: WS msg not a valid CallRequestDTO',
          //   request,
          //   e,
          // )

          return
        }

        if (request) {
          // debugMessages('handling received request')

          if (request.identity) {
            if (!isIdentityValid(request.identity)) {
              console.log(
                'error recieving CallResponseDTO with invalid RPCClientIdentity schema',
                request.identity,
              )

              return
            }

            identity = request.identity
          }

          const response = await this.handleIncomingRequest(request)

          // debugMessages('returning request response')

          try {
            ws.send(JSON.stringify(CallResponse.toCallResponseDTO(response)))
          } catch (e: any) {
            console.error(
              'WebSocketServer error: error sending CallResponseDTO to WS connection',
              response,
              e,
            )
          }

          return
        }
      })
    })

    this.wss.once('listening', () => {
      debug(
        `${this.options.config.displayName} listening ${this.host}:${this.port}`,
      )

      if (onStartedCallback) {
        onStartedCallback()
      }
    })
  }

  stop = (onStoppedCallback?: () => void) => {
    this.wssShuttingDown = true

    this.wss?.close((err) => {
      if (err) {
        console.log('WSS error onclose:', err)
      }
      if (onStoppedCallback) onStoppedCallback()
    })

    for (const ws of this.wss?.clients ?? []) {
      ws.terminate()
    }
  }

  private handleIncomingRequest = async (
    request: CallRequest,
  ): Promise<CallResponse> => {
    try {
      debug('sending request to requestHandler')

      return await this.requestHandler!(request as CallRequest)
    } catch (e: any) {
      if (e instanceof CallResponse) {
        debug('handleIncomingRequest catching a throw CallResponse')

        return e as CallResponse
      }

      console.log(
        'WebSocketServer error: there was an error handling request:',
        request,
        e,
      )

      return new CallResponse(
        {
          code: 500,
          message: 'There was a problem calling the procedure',
          success: false,
        },
        request,
      )
    }
  }
}
