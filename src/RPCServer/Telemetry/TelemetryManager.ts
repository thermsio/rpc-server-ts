import os from 'os'
import {
  ICallRequestDescription,
} from '../../CallRequest'
import { HandlerRegistrationInfo } from '../../Handler'
import { RPCServerConfig } from '../RPCServer'
import { ITelemetryHeartbeat } from '../../Telemetry'
import { TelemetryDataManager } from '../../TelemetryServer/intrinsic/TelemetryDataManager'
import Timeout = NodeJS.Timeout

interface ITelemetryInfoManagerOptions {
  config: RPCServerConfig
}

export class TelemetryManager {
  private heartbeatData: ITelemetryHeartbeat = {
    hostName: os.hostname(),
    ephemeralId: '',
    gatewayHttpServer: false,
    gatewayWebSocketServer: false,
    displayName: '',
    startTime: '',
  }
  private heartbeatInterval?: Timeout
  private registeredHandlers: HandlerRegistrationInfo[] = []
  public readonly telemetryDataManager: TelemetryDataManager

  constructor(readonly options: ITelemetryInfoManagerOptions) {
    this.telemetryDataManager = new TelemetryDataManager(
      options.config.telemetry?.adapter,
    )

    this.heartbeatData.ephemeralId = options.config.ephemeralId || new Date().toISOString()
    this.heartbeatData.gatewayHttpServer = !!options.config.server?.http
    this.heartbeatData.gatewayWebSocketServer =
      !!options.config.server?.websocket
    this.heartbeatData.displayName = options.config.displayName
    this.heartbeatData.startTime = new Date().toISOString()
  }

  getHeartbeatData = () => this.heartbeatData

  getRegisteredHandlers = () => this.registeredHandlers

  registerHandler = (request: ICallRequestDescription) => {
    this.registeredHandlers.push(request)
  }

  startTelemetryReporting = async () => {
    this.heartbeatInterval = setInterval(() => {
      this.saveHeartbeat()
    }, 30000)

    await this.saveHeartbeat()

    await this.telemetryDataManager.saveHandlersRegistrationInfo(
      this.registeredHandlers.map((request) => ({
        ...request,
        createdAt: new Date().toISOString(),
        serverDisplayName: this.options.config.displayName,
      })),
    )
  }

  private saveHeartbeat = async () => {
    return this.telemetryDataManager.saveHeartbeat(this.heartbeatData)
  }
}
