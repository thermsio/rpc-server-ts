import { MessageBrokerManager } from './MessageBrokerManager'
import { CallResponse } from '../../../../CallResponse'

describe('MessageBrokerManager', () => {
  it('can instantiate', () => {
    const messageBrokerManager = new MessageBrokerManager({
      config: {
        displayName: 'test',
        ephemeralId: Math.random().toString(),
        messageBroker: {
          amqpURI: 'amqp://localhost:5672',
        },
      },

      requestHandler: async () => Promise.resolve(CallResponse.EMPTY),
    })

    expect(messageBrokerManager).toBeDefined()
  })
})
