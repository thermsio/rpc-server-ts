export type TelemetryHeartbeatDoc = {
    createdAt: string
    displayName: string
    ephemeralId: string
    gatewayHttpServer: boolean
    gatewayWebSocketServer: boolean
    hostName: string
    startTime: string
}

export type TelemetryRPCDoc = {
    args?: any
    createdAt: string
    data?: any
    description?: string
    identity?: string[] | string
    internal?: boolean
    procedure: string
    scope: string
    serverDisplayName: string
    version: string
}
