import { TelemetryServer, TelemetryServerConfig } from './TelemetryServer'
import axios from 'axios'

describe('TelemetryServer class', () => {
  let telemetryServer: TelemetryServer
  let config: TelemetryServerConfig = {
    adapter: {},
    debug: false,
    server: { host: 'localhost', port: 4300 },
  }

  beforeEach(() => {
    telemetryServer = new TelemetryServer(config)
  })

  test('correctly starts the telemetryHttpServer', async () => {
    await telemetryServer.start()
    await expect(axios.get('http://localhost:4300/docs')).resolves.not.toThrow()
    telemetryServer.stop()
  })
})
