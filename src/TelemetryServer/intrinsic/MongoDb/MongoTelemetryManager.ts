import { uniqBy } from 'lodash'
import { ITelemetryHeartbeat } from '../../../Telemetry'
import { HeartbeatModel, RPCDocModel } from './Models'
import { MongoManager } from './MongoManager'
import { TelemetryRPCDoc } from '../../types'

export class MongoTelemetryManager {
  private mongoManager: MongoManager

  constructor(adaptorConfig: { mongoURI: string }) {
    this.mongoManager = new MongoManager(adaptorConfig.mongoURI)
    this.mongoManager.setupMongoConnection()
  }

  disconnect = () => {
    this.mongoManager.tearDownConnection()
  }

  getHeartbeats = async (): Promise<any> => {
    return HeartbeatModel.find({}, null, { limit: 500 })
      .lean()
      .exec()
      .then((docs) => {
        return uniqBy(docs, 'ephemeralId')
      })
  }

  getProcedureDocs = async (): Promise<any> => {
    return RPCDocModel.find({}, null, { limit: 5000 }).lean().exec()
  }

  saveHeartbeat = async (telemetryHeartbeat: ITelemetryHeartbeat) => {
    const doc = new HeartbeatModel(telemetryHeartbeat)

    try {
      await doc.save()
    } catch (err) {
      return console.error('MongoTelemetryManager#saveHeartbeat error:', err)
    }
  }

  saveHandlersRegistrationInfo = async (
    procedureDescriptions: TelemetryRPCDoc[],
  ) => {
    if (!procedureDescriptions.length) return

    let waitCount = 0

    while (!this.mongoManager.connected) {
      if (waitCount > 30) {
        throw Error('MongoManager has not connected to mongoDB')
      }

      waitCount++
      await new Promise((r) => setTimeout(r, 100))
    }

    try {
      const serverDisplayName = procedureDescriptions[0].serverDisplayName

      await RPCDocModel.deleteMany({ serverDisplayName })

      await RPCDocModel.insertMany(procedureDescriptions)
    } catch (err) {
      console.error(
        'MongoTelemetryManager#saveHandlerRegistrationInfo error:',
        err,
      )
    }
  }
}
