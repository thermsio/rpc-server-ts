import { TelemetryHeartbeatDoc, TelemetryRPCDoc } from '../types'
import { MongoTelemetryManager } from './MongoDb/MongoTelemetryManager'
import { ITelemetryHeartbeat } from '../../Telemetry'

export class TelemetryDataManager {
  private inMemoryRPCDocs: TelemetryRPCDoc[] = []
  private mongoTelemetryManager?: MongoTelemetryManager

  /**
   * If an adaptor is not provided, the TelemetryDataManager will use an in-memory store.
   * @param adaptor
   */
  constructor(adaptor?: { mongoURI?: string }) {
    if (adaptor?.mongoURI) {
      this.mongoTelemetryManager = new MongoTelemetryManager({
        mongoURI: adaptor.mongoURI,
      })
    }
  }

  disconnect = () => {
    this.mongoTelemetryManager?.disconnect()
  }

  getHeartbeats = async (): Promise<TelemetryHeartbeatDoc[]> => {
    if (this.mongoTelemetryManager) {
      return this.mongoTelemetryManager.getHeartbeats()
    }

    return []
  }

  getRPCDocs = async (): Promise<TelemetryRPCDoc[]> => {
    if (this.mongoTelemetryManager) {
      return this.mongoTelemetryManager.getProcedureDocs()
    }

    return this.inMemoryRPCDocs
  }

  saveHandlersRegistrationInfo = async (
    procedureDescriptions: TelemetryRPCDoc[],
  ): Promise<void> => {
    if (!procedureDescriptions.length) return

    if (this.mongoTelemetryManager) {
      await this.mongoTelemetryManager.saveHandlersRegistrationInfo(
        procedureDescriptions,
      )
    } else {
      this.inMemoryRPCDocs = this.inMemoryRPCDocs.concat(procedureDescriptions)
    }
  }

  saveHeartbeat = async (heartbeat: ITelemetryHeartbeat): Promise<void> => {
    if (this.mongoTelemetryManager) {
      await this.mongoTelemetryManager.saveHeartbeat(heartbeat)
    }
  }
}
