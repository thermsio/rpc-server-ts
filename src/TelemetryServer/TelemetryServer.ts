import { deepFreeze } from '../utils/deep-freeze'
import { TelemetryDataManager } from './intrinsic/TelemetryDataManager'
import { TelemetryHttpServer } from './TelemetryHttpServer'
import Koa from 'koa'

export interface TelemetryServerConfig {
  adapter: {
    mongoURI?: string
  }

  debug?: boolean

  /**
   * The server configuration. Optionally provide a Koa instance to be extended, otherwise, a new Koa instance will
   * be created.
   */
  server:
    | {
        host?: string
        port: number
        staticFilesPath?: string
      }
    | Koa
}

export class TelemetryServer {
  private config: TelemetryServerConfig
  private telemetryHttpServer?: TelemetryHttpServer
  private telemetryDataManager: TelemetryDataManager

  constructor(config: TelemetryServerConfig) {
    this.config = deepFreeze<TelemetryServerConfig>(config)

    this.telemetryDataManager = new TelemetryDataManager(config.adapter)

    this.telemetryHttpServer = new TelemetryHttpServer({
      config: config,
      telemetryDataManager: this.telemetryDataManager,
    })
  }

  start = async () => {
    this?.telemetryHttpServer?.start()
  }

  stop = () => {
    this.telemetryDataManager?.disconnect()
    this?.telemetryHttpServer?.stop()
  }
}
