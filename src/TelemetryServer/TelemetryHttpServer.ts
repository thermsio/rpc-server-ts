import Koa from 'koa'
import cors from '@koa/cors'
import logger from 'koa-logger'
// @ts-ignore
import { Server as NodeHttpServer } from 'http'
import { TelemetryServerConfig } from './TelemetryServer'
import { TelemetryDataManager } from './intrinsic/TelemetryDataManager'
import { findDirectoryUpTree } from '../utils/find-dir-up-tree'
import fs from 'fs'

interface IHttpServerOptions {
  config: TelemetryServerConfig
  telemetryDataManager: TelemetryDataManager
}

const DEFAULT_HOST = '127.0.0.1'

export class TelemetryHttpServer {
  static configureKoaAppRoutes = (
    app: Koa,
    telemetryDataManager: TelemetryDataManager,
  ) => {
    app.use(async (ctx, next) => {
      if (ctx.method !== 'GET') return await next()

      switch (ctx.request.path) {
        case '/':
          ctx.status = 307
          ctx.redirect(
            `https://rpc-docs.dev/?server=${ctx.request.origin || ctx.request.originalUrl || ctx.request.protocol + '://' + ctx.request.host}`,
          )
          return
        case '/docs':
          try {
            ctx.body = await telemetryDataManager.getRPCDocs()
            ctx.status = 200
          } catch (e: any) {
            console.log('TelemetryHttpServer error fetching procedure docs:', e)
            ctx.status = 500
          }
          return

        case '/health':
          ctx.body = 'ok'
          ctx.status = 200
          return

        case '/heartbeats':
          try {
            const docs = await telemetryDataManager.getHeartbeats()
            ctx.body = docs
            ctx.status = 200
          } catch (e: any) {
            console.log('TelemetryHttpServer error fetching heartbeats:', e)
            ctx.status = 500
          }
          return
      }

      return await next()
    })
  }

  private koaApp: Koa
  private koaAppListener?: NodeHttpServer
  private telemetryDataManager: TelemetryDataManager

  constructor(readonly options: IHttpServerOptions) {
    this.koaApp = new Koa()
    this.koaApp.use(cors())
    this.koaApp.use(logger())

    this.telemetryDataManager = options.telemetryDataManager
  }

  start = (onStartedCallback?: () => void) => {
    if (this.koaAppListener) return

    TelemetryHttpServer.configureKoaAppRoutes(
      this.koaApp,
      this.telemetryDataManager,
    )

    if ('host' in this.options.config.server) {
      const host = this.options.config.server?.host || DEFAULT_HOST
      const port = this.options.config.server.port

      this.koaAppListener = this.koaApp.listen(
        {
          host,
          port,
        },
        () => {
          this.log(`Telemetry HTTP server listening ${host}:${port}`)

          if (onStartedCallback) {
            onStartedCallback()
          }
        },
      )
    }
  }

  stop = (onStoppedCallback?: () => void) => {
    this.koaAppListener?.close((err) => {
      if (err) {
        console.error(`telemetry server had an error when stopping`, err)
      } else {
        this.log(`telemetry server stopped`)
        this.koaAppListener = undefined

        if (onStoppedCallback) {
          onStoppedCallback()
        }
      }
    })
  }

  private log = (...args: any[]) => {
    console.log(`TelemetryServer `, ...args)
  }
}
