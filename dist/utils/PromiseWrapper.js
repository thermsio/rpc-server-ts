const DEFAULT_TIMEOUT = 30000;
export class PromiseTimeoutError extends Error {
}
export class PromiseWrapper {
    constructor(timeout) {
        this.reject = (rejectReturnValue) => {
            if (this.rejectPromise) {
                this.rejectPromise(rejectReturnValue);
            }
        };
        this.resolve = (resolveReturnValue) => {
            if (this.resolvePromise) {
                this.resolvePromise(resolveReturnValue);
            }
        };
        this.promise = new Promise((resolve, reject) => {
            const timer = setTimeout(() => {
                reject(new PromiseTimeoutError('PromiseWraper timeout'));
            }, timeout || DEFAULT_TIMEOUT);
            this.rejectPromise = (arg) => {
                clearTimeout(timer);
                reject(arg);
            };
            this.resolvePromise = (arg) => {
                clearTimeout(timer);
                resolve(arg);
            };
        });
    }
}
//# sourceMappingURL=PromiseWrapper.js.map