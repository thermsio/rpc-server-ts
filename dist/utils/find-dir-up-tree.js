import fs from 'fs';
import path from 'path';
/**
 * Checks if the directory exists.
 * @param {string} dirPath - The path to the directory to check.
 * @returns {boolean} - True if the directory exists, false otherwise.
 */
function directoryExists(dirPath) {
    try {
        return fs.statSync(dirPath).isDirectory();
    }
    catch (err) {
        return false;
    }
}
/**
 * Traverses up from the current directory to find a specific directory.
 * @param {string} targetDirPath - The name of the target directory to find.
 * @param {string} currentDir - The current directory to start from.
 * @returns {string|null} - The path to the found directory, or null if not found.
 */
export function findDirectoryUpTree(targetDirPath, currentDir = __dirname) {
    let dir = currentDir;
    const root = path.parse(dir).root;
    while (true) {
        const potentialPath = path.join(dir, targetDirPath);
        if (directoryExists(potentialPath)) {
            return potentialPath;
        }
        if (dir === root) {
            break;
        }
        dir = path.dirname(dir);
    }
    return null;
}
//# sourceMappingURL=find-dir-up-tree.js.map