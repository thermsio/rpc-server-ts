import ObjectId from 'bson-objectid';
export const DEFAULT_REQUEST_SCOPE = 'global';
export const DEFAULT_REQUEST_VERSION = '1';
export class CallRequest {
    /**
     * Check if an object is a valid call request
     * @param obj
     */
    static isCallRequest(obj) {
        if (typeof obj === 'object') {
            return (obj.hasOwnProperty('procedure') &&
                obj.hasOwnProperty('scope') &&
                obj.hasOwnProperty('version') &&
                obj.hasOwnProperty('trace'));
        }
        return false;
    }
    constructor(request) {
        this.args = request.args;
        this.correlationId = request.correlationId;
        this.identity = request.identity;
        this.procedure = request.procedure;
        this.scope = request.scope || DEFAULT_REQUEST_SCOPE;
        this.version = request.version || DEFAULT_REQUEST_VERSION;
        this.trace = request.trace;
    }
}
CallRequest.fromCallRequestDTO = (callRequestDTO, details) => {
    var _a;
    if (!callRequestDTO.procedure) {
        throw new Error('procedure is required');
    }
    else if (!((_a = details === null || details === void 0 ? void 0 : details.trace) === null || _a === void 0 ? void 0 : _a.caller)) {
        throw new Error('trace.caller is required');
    }
    return new CallRequest({
        args: callRequestDTO.args,
        correlationId: callRequestDTO.correlationId,
        identity: callRequestDTO.identity,
        procedure: callRequestDTO.procedure,
        scope: callRequestDTO.scope,
        version: callRequestDTO.version,
        trace: {
            caller: details.trace.caller,
            id: callRequestDTO.correlationId || new ObjectId().toString(),
            internal: !!details.trace.internal,
            ipAddress: details.trace.ipAddress,
        },
    });
};
CallRequest.EMPTY = new CallRequest({
    procedure: 'empty',
    trace: {
        caller: 'empty',
        id: '0',
    },
});
CallRequest.toCallRequestDescription = (request) => {
    return {
        procedure: request.procedure,
        scope: request.scope || DEFAULT_REQUEST_SCOPE,
        version: request.version || DEFAULT_REQUEST_VERSION,
    };
};
//# sourceMappingURL=CallRequest.js.map