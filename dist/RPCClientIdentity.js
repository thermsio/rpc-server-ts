export function isIdentityValid(identity) {
    if (!identity)
        return false;
    if (identity.authorization && typeof identity.authorization !== 'string')
        return false;
    if (identity.deviceName && typeof identity.deviceName !== 'string')
        return false;
    if (identity.metadata && typeof identity.metadata !== 'object')
        return false;
    return true;
}
//# sourceMappingURL=RPCClientIdentity.js.map