var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { EventBus } from './EventBus';
describe('EventBus', () => {
    it('can instantiate', () => {
        new EventBus();
    });
    it('can subscribe/publish', (done) => {
        const eventBus = new EventBus();
        eventBus.subscribe('TEST', (str) => {
            expect(str).toEqual('str');
            done();
        });
        setTimeout(() => {
            eventBus.publish({ topic: 'TEST', payload: 'str' });
        }, 1);
    });
    it('can unsubscribe', (done) => {
        const eventBus = new EventBus();
        const handler = jest.fn((str) => {
            expect(str).toEqual('str');
        });
        const { unsubscribe } = eventBus.subscribe('TEST', handler);
        unsubscribe();
        setTimeout(() => {
            eventBus.publish({ topic: 'TEST', payload: 'str' });
            setTimeout(() => {
                expect(handler.mock.calls.length).toEqual(0);
                done();
            }, 1);
        }, 1);
    });
    it('can publish async', (done) => {
        const eventBus = new EventBus();
        const handler = jest.fn((str) => __awaiter(void 0, void 0, void 0, function* () {
            yield Promise.resolve(str);
        }));
        eventBus.subscribe('TEST', handler);
        setTimeout(() => __awaiter(void 0, void 0, void 0, function* () {
            yield eventBus.publishAsync({ topic: 'TEST', payload: 'str' });
            expect(handler.mock.calls.length).toEqual(1);
            done();
        }), 1);
    });
    it('can publish sync', (done) => {
        const eventBus = new EventBus();
        eventBus.subscribe('TEST', (str) => {
            expect(str).toEqual('str');
            done();
        });
        setTimeout(() => {
            eventBus.publishSync({ topic: 'TEST', payload: 'str' });
        }, 1);
    });
});
//# sourceMappingURL=EventBus.test.js.map