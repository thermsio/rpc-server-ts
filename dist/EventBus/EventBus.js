var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
export class EventBus {
    constructor() {
        this.topics = {};
        this.publish = (event) => {
            if (!this.topics.hasOwnProperty.call(this.topics, event.topic)) {
                return;
            }
            // we make the forEach cb async to prevent blocking the vent loop
            this.topics[event.topic].forEach((item) => __awaiter(this, void 0, void 0, function* () {
                item(event.payload);
            }));
        };
        this.publishAsync = (event) => {
            if (!this.topics.hasOwnProperty.call(this.topics, event.topic)) {
                return Promise.resolve();
            }
            const promises = [];
            this.topics[event.topic].forEach((topicSubscriptionHandler) => {
                const returned = topicSubscriptionHandler(event.payload);
                if (returned && 'then' in returned)
                    promises.push(returned);
            });
            return Promise.all(promises).then(() => undefined);
        };
        this.publishSync = (event) => {
            if (!this.topics.hasOwnProperty.call(this.topics, event.topic)) {
                return;
            }
            this.topics[event.topic].forEach((item) => {
                item(event.payload);
            });
        };
        this.subscribe = (topic, handler) => {
            if (!this.topics.hasOwnProperty.call(this.topics, topic)) {
                this.topics[topic] = [];
            }
            const index = this.topics[topic].push(handler) - 1;
            return {
                unsubscribe: () => {
                    delete this.topics[topic][index];
                },
            };
        };
        this.unsubscribe = (event, handler) => {
            if (!this.topics.hasOwnProperty.call(this.topics, event.topic)) {
                this.topics[event.topic] = [];
            }
            this.topics[event.topic].forEach((topicSubscriptionHandler, index) => {
                if (topicSubscriptionHandler === handler) {
                    delete this.topics[event.topic][index];
                }
            });
        };
    }
}
//# sourceMappingURL=EventBus.js.map