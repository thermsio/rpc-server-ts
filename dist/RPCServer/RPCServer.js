var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { CallRequest, } from '../CallRequest';
import { CallManager } from './CallManager/CallManager';
import { HttpServer } from './GatewayServers/HttpServer';
import { WebSocketServer, } from './GatewayServers/WebSocketServer';
import { deepFreeze } from '../utils/deep-freeze';
import { TelemetryManager } from './Telemetry/TelemetryManager';
import ObjectId from 'bson-objectid';
import { CallResponse } from '../CallResponse';
import Debug from 'debug';
import { makeHandlerRequestKey } from './CallManager/intrinsic/utils/make-handler-request-key';
const debug = Debug('rpc:RPCServer');
export class RPCServer {
    constructor(config) {
        this.serverStarted = false;
        this.call = (request, traceCaller) => __awaiter(this, void 0, void 0, function* () {
            if (!traceCaller) {
                throw Error('RPCServer.call() requires "traceCaller" param string to track where this call originated');
            }
            const response = yield this.callManager.manageRequest(CallRequest.fromCallRequestDTO(request, {
                trace: {
                    caller: `RPCServer.call ${traceCaller}`,
                    internal: true,
                },
            }));
            if (!response.success) {
                throw response;
            }
            return response;
        });
        this.getRegisteredHandlers = () => this.callManager.getRegisteredHandlers();
        /**
         * Register a handler for this RPCServer to handle and manage requests to/from.
         *
         * The handler callback provides an "call" method for registered handlers to make calls to other RPC handlers in
         * the same network. The incoming CallRequest's RPCClientIdentity will be used when making subsequent calls.
         *
         * @param request
         * @param handler
         */
        this.registerHandler = (request, handler) => {
            var _a;
            if (this.serverStarted) {
                throw Error('RPCServer: you cannot register a handler after the server has started.');
            }
            // wrap the handler so handler implementations can return a POJO
            const wrappedHandler = (request) => __awaiter(this, void 0, void 0, function* () {
                var _b;
                try {
                    let callResponse;
                    const originalRequest = request;
                    const reqKey = makeHandlerRequestKey(originalRequest);
                    debug(`wrappedHandler, Handler -> ${reqKey}`, {
                        args: request.args,
                        identity: request.identity,
                        trace: request.trace,
                    });
                    const internalCallWithCallerIdentity = (request) => {
                        return this.call(Object.assign({ identity: originalRequest.identity }, request), makeHandlerRequestKey(originalRequest));
                    };
                    const handlerResponse = yield handler(request, internalCallWithCallerIdentity);
                    if (handlerResponse instanceof CallResponse) {
                        callResponse = handlerResponse;
                    }
                    else if (typeof handlerResponse === 'object' &&
                        handlerResponse !== null &&
                        'code' in handlerResponse &&
                        'success' in handlerResponse) {
                        callResponse = new CallResponse(handlerResponse, request);
                    }
                    else {
                        console.log(new Error(`${reqKey} handler did not return a valid CallResponse or CallResponseDTO, "success" and "code" properties are required.`));
                        throw {
                            code: 500,
                            message: `RPC handler error`,
                            success: false,
                        };
                    }
                    if (!callResponse.success) {
                        throw callResponse;
                    }
                    return callResponse;
                }
                catch (e) {
                    debug(`wrappedHandler, caught error for request ${makeHandlerRequestKey(request)}`, e);
                    if (CallResponse.isCallResponse(e)) {
                        return e;
                    }
                    else if (CallResponse.isCallResponseDTO(e)) {
                        return new CallResponse(e, request);
                    }
                    else if ((_b = this.config.handlers) === null || _b === void 0 ? void 0 : _b.onHandlerError) {
                        try {
                            const onHandlerErrorResult = this.config.handlers.onHandlerError(e, request);
                            if (onHandlerErrorResult) {
                                if (onHandlerErrorResult instanceof CallResponse) {
                                    return onHandlerErrorResult;
                                }
                                if (CallResponse.isCallResponseDTO(onHandlerErrorResult)) {
                                    return new CallResponse(onHandlerErrorResult, request);
                                }
                            }
                        }
                        catch (e) {
                            console.log('LocalHandlerManager catch and call RPCService optional onHandlerError() error', e);
                        }
                    }
                    console.log('RPCServer wrappedHandler error: ', e, request);
                    return new CallResponse({ code: 500, message: 'server handler issue', success: false }, request);
                }
            });
            this.callManager.registerHandler(request, wrappedHandler);
            (_a = this.telemetryManager) === null || _a === void 0 ? void 0 : _a.registerHandler(request);
        };
        /**
         * Send a message to a specific RPC client by connectionId.
         * @param connectionId
         * @param data
         */
        this.sendMessageToClient = (connectionId, data) => {
            if (this.websocketServer) {
                this.websocketServer.sendMessageToClient(connectionId, data);
            }
        };
        this.start = () => {
            var _a, _b, _c;
            if (!this.serverStarted) {
                this.serverStarted = true;
                (_a = this === null || this === void 0 ? void 0 : this.httpServer) === null || _a === void 0 ? void 0 : _a.start();
                (_b = this === null || this === void 0 ? void 0 : this.websocketServer) === null || _b === void 0 ? void 0 : _b.start();
                (_c = this === null || this === void 0 ? void 0 : this.telemetryManager) === null || _c === void 0 ? void 0 : _c.startTelemetryReporting();
            }
            else {
                throw Error('RPCServer: cannot call start() more than once');
            }
        };
        this.stop = () => {
            var _a, _b;
            (_a = this === null || this === void 0 ? void 0 : this.httpServer) === null || _a === void 0 ? void 0 : _a.stop();
            (_b = this === null || this === void 0 ? void 0 : this.websocketServer) === null || _b === void 0 ? void 0 : _b.stop();
        };
        this.setupGatewayServer = () => {
            var _a, _b;
            const config = this.config;
            if (!(config === null || config === void 0 ? void 0 : config.messageBroker) && !config.server) {
                debug('no config.messageBroker or config.gatewayServer options, acting as standalone server');
            }
            if ((_a = config === null || config === void 0 ? void 0 : config.server) === null || _a === void 0 ? void 0 : _a.http) {
                this.httpServer = new HttpServer({
                    config: this.config,
                    telemetryManager: this.telemetryManager,
                });
                this.httpServer.setIncomingRequestHandler(this.callManager.manageRequest);
            }
            if ((_b = config === null || config === void 0 ? void 0 : config.server) === null || _b === void 0 ? void 0 : _b.websocket) {
                this.websocketServer = new WebSocketServer({
                    config: this.config,
                });
                this.websocketServer.setIncomingRequestHandler(this.callManager.manageRequest);
            }
        };
        if (!config.ephemeralId) {
            config.ephemeralId = new ObjectId().toString();
        }
        this.config = deepFreeze(config);
        if (!config.displayName) {
            throw new Error('config.displayName is required');
        }
        this.telemetryManager = new TelemetryManager({
            config: this.config,
        });
        this.callManager = new CallManager({
            config: this.config,
        });
        debug(`RPCServer id: ${this.config.ephemeralId}`);
        this.setupGatewayServer();
    }
}
//# sourceMappingURL=RPCServer.js.map