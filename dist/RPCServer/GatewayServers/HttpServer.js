var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import Http from 'http';
import Koa from 'koa';
import cors from '@koa/cors';
import koaBodyParser from 'koa-bodyparser';
import { DEFAULT_BIND } from './Server';
import { CallRequest } from '../../CallRequest';
import { CallResponse } from '../../CallResponse';
import Debug from 'debug';
import { makeHandlerRequestKey } from '../CallManager/intrinsic/utils/make-handler-request-key';
import { TelemetryHttpServer } from '../../TelemetryServer/TelemetryHttpServer';
const debug = Debug('rpc:TelemetryHttpServer');
export class HttpServer {
    constructor(options) {
        var _a, _b;
        this.options = options;
        this.serverListening = false;
        this.name = 'HttpServer';
        this.setIncomingRequestHandler = (requestHandler) => {
            this.requestHandler = requestHandler;
        };
        this.start = (onStartedCallback) => {
            var _a, _b, _c, _d;
            if (!this.requestHandler) {
                throw new Error(`No request handler has be set`);
            }
            if (this.serverListening) {
                debug('cannot start again, already runnning');
                return;
            }
            const host = ((_b = (_a = this.options.config.server) === null || _a === void 0 ? void 0 : _a.http) === null || _b === void 0 ? void 0 : _b.host) || DEFAULT_BIND;
            const port = (_d = (_c = this.options.config.server) === null || _c === void 0 ? void 0 : _c.http) === null || _d === void 0 ? void 0 : _d.port;
            this.httpServer.listen({
                host,
                port,
            }, () => {
                debug(`${this.options.config.displayName} listening ${host}:${port}`);
                this.serverListening = true;
                if (onStartedCallback) {
                    onStartedCallback();
                }
            });
        };
        this.stop = (onStoppedCallback) => {
            var _a, _b, _c, _d, _e;
            const host = ((_b = (_a = this.options.config.server) === null || _a === void 0 ? void 0 : _a.http) === null || _b === void 0 ? void 0 : _b.host) || DEFAULT_BIND;
            const port = (_d = (_c = this.options.config.server) === null || _c === void 0 ? void 0 : _c.http) === null || _d === void 0 ? void 0 : _d.port;
            (_e = this.httpServer) === null || _e === void 0 ? void 0 : _e.close((err) => {
                if (err) {
                    debug(`error when stopping (${host}:${port})`, err);
                }
                else {
                    debug(`stopped (${host}:${port})`);
                    this.serverListening = false;
                }
                if (onStoppedCallback) {
                    onStoppedCallback();
                }
            });
        };
        this.setup = () => {
            this.koaApp.use((ctx) => __awaiter(this, void 0, void 0, function* () {
                debug('received request');
                if (ctx.request.path === '/health') {
                    debug('returning health check status');
                    ctx.body = 'ok';
                    ctx.status = 200;
                    return;
                }
                if (!ctx.request.body ||
                    typeof ctx.request.body !== 'object' ||
                    !Object.keys(ctx.request.body).length) {
                    debug('empty request body received', ctx.request.body);
                    ctx.body = {
                        code: 400,
                        message: 'Empty request body received',
                        success: false,
                    };
                    return;
                }
                const requestDTO = ctx.request.body;
                let request;
                try {
                    request = CallRequest.fromCallRequestDTO(requestDTO, {
                        trace: {
                            caller: 'HttpServer',
                            ipAddress: HttpServer.parseIPAddressFromHttpReq(ctx.req),
                        },
                    });
                    debug(`request "${makeHandlerRequestKey(request)}"`);
                }
                catch (e) {
                    debug('error converting request body to CallRequest', e);
                    ctx.body = {
                        code: 400,
                        correlationId: request === null || request === void 0 ? void 0 : request.correlationId,
                        message: 'unable to parse request body as a valid CallRequest',
                        success: false,
                    };
                    return;
                }
                try {
                    debug('sending request to requestHandler');
                    const response = yield this.requestHandler(request);
                    debug('returning request response');
                    ctx.body = CallResponse.toCallResponseDTO(response);
                }
                catch (e) {
                    console.log('HttpSerer error: there was an error handling request:', e, request);
                    ctx.body = {
                        code: 500,
                        correlationId: request === null || request === void 0 ? void 0 : request.correlationId,
                        message: 'There was a problem calling the procedure',
                        success: false,
                    };
                }
                return;
            }));
        };
        this.koaApp = new Koa({ proxy: true });
        this.koaApp.use(koaBodyParser());
        this.koaApp.use(cors({ maxAge: 600, origin: '*' }));
        this.koaApp.use((ctx, next) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield next();
            }
            catch (err) {
                console.log('There was an error in the Koa app from an incoming request', err);
                ctx.body = {
                    code: 400,
                    message: 'unable to parse & handle request',
                    success: false,
                };
            }
        }));
        if (((_b = (_a = options.config.server) === null || _a === void 0 ? void 0 : _a.http) === null || _b === void 0 ? void 0 : _b.serveLocalTelemetryClient) &&
            options.telemetryManager) {
            TelemetryHttpServer.configureKoaAppRoutes(this.koaApp, options.telemetryManager.telemetryDataManager);
        }
        this.httpServer = Http.createServer(this.koaApp.callback());
        this.setup();
    }
    static parseIPAddressFromHttpReq(req) {
        let ip = '';
        if (req.headers['x-real-ip']) {
            if (Array.isArray(req.headers['x-real-ip'])) {
                ip = req.headers['x-real-ip'][0];
            }
            else {
                ip = req.headers['x-real-ip'].split(',')[0];
            }
        }
        else {
            ip = req.socket.remoteAddress || '';
        }
        ip = ip.trim();
        return ip;
    }
}
//# sourceMappingURL=HttpServer.js.map