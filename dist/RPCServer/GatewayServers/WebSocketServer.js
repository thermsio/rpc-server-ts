var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import WsServer from 'ws';
import { DEFAULT_BIND } from './Server';
import Debug from 'debug';
import { CallRequest } from '../../CallRequest';
import { CallResponse } from '../../CallResponse';
import { isIdentityValid } from '../../RPCClientIdentity';
import uuid from 'node-uuid';
import { isEqual } from 'lodash';
const debug = Debug('rpc:WebSocketServer');
export class WebSocketServer {
    constructor(options) {
        var _a, _b, _c, _d, _e, _f;
        this.options = options;
        this.clientMessageHandlers = [];
        this.connectionsByConnectionId = new Map();
        this.name = 'WebSocketServer';
        /**
         * This method allows adding a callback for websocket client messages to be handled that are not RPC requests.
         * @param handler
         */
        this.addClientMessageHandler = (handler) => {
            this.clientMessageHandlers.push(handler);
        };
        this.removeClientMessageHandler = (handler) => {
            const indexToRemove = this.clientMessageHandlers.findIndex((func) => func === handler);
            if (indexToRemove > -1) {
                this.clientMessageHandlers.splice(indexToRemove, 1);
            }
        };
        this.setIncomingRequestHandler = (requestHandler) => {
            this.requestHandler = requestHandler;
        };
        this.sendMessageToClient = (connectionId, data) => {
            const ws = this.connectionsByConnectionId.get(connectionId);
            if (ws) {
                let serverMessage;
                try {
                    serverMessage = JSON.stringify({ serverMessage: data });
                }
                catch (e) {
                    console.log('WebSocketServer.sendMessageToClient() error stringify serverMessage to send to client: ', serverMessage);
                }
                if (serverMessage) {
                    ws.send(serverMessage);
                }
            }
        };
        this.start = (onStartedCallback) => {
            if (!this.requestHandler) {
                throw new Error(`No request handler has be set`);
            }
            this.wss = new WsServer.Server({
                host: this.host,
                port: this.port,
            });
            this.wss.on('connection', (ws, req) => {
                var _a, _b, _c;
                const websocketId = uuid.v4();
                this.connectionsByConnectionId.set(websocketId, ws);
                debug('websocket connected, connectionId:', websocketId);
                let identity;
                const wsIp = WebSocketServer.parseIPAddressFromWsReq(req);
                if ((_c = (_b = (_a = this.options.config) === null || _a === void 0 ? void 0 : _a.server) === null || _b === void 0 ? void 0 : _b.websocket) === null || _c === void 0 ? void 0 : _c.onClientConnect) {
                    this.options.config.server.websocket.onClientConnect({
                        connectionId: websocketId,
                        identity,
                        ip: wsIp,
                    });
                }
                ws.on('close', (code, data) => {
                    var _a, _b, _c;
                    this.connectionsByConnectionId.delete(websocketId);
                    const reason = data.toString();
                    debug('websocket disconnected, connectionId:', websocketId, reason);
                    if ((_c = (_b = (_a = this.options.config) === null || _a === void 0 ? void 0 : _a.server) === null || _b === void 0 ? void 0 : _b.websocket) === null || _c === void 0 ? void 0 : _c.onClientDisconnect) {
                        this.options.config.server.websocket.onClientDisconnect({
                            connectionId: websocketId,
                            identity,
                            ip: wsIp,
                        });
                    }
                });
                // when there is a client connection we need to handle it so it's not fatal
                ws.on('error', console.error);
                ws.on('message', (data, isBinary) => __awaiter(this, void 0, void 0, function* () {
                    var _d, _e, _f;
                    const message = isBinary ? data : data.toString();
                    debug('received message from WS, connectionId:', websocketId, message);
                    let messageParsed;
                    if (typeof message === 'string') {
                        try {
                            messageParsed = JSON.parse(message);
                        }
                        catch (e) {
                            debug('WebSocketServer error: received unparseable WS msg string', message);
                            return;
                        }
                    }
                    else {
                        debug('WebSocketServer error: received unknown "message" from client', message);
                    }
                    const incomingMessage = messageParsed;
                    if (incomingMessage.identity) {
                        if (!isIdentityValid(incomingMessage.identity)) {
                            console.log('error receiving CallResponseDTO with invalid identity schema', incomingMessage.identity);
                            return;
                        }
                        if ((identity === null || identity === void 0 ? void 0 : identity.authorization) &&
                            identity.authorization !== incomingMessage.identity.authorization) {
                            console.log('error, a websocket changed identity.authorization from previous value, disconnecting...');
                            ws.send(JSON.stringify(CallResponse.toCallResponseDTO(new CallResponse({
                                code: 400,
                                message: 'identity.authorization has changed - disconnecting websocket connection',
                                success: false,
                            }, CallRequest.fromCallRequestDTO(Object.assign(Object.assign({}, incomingMessage), { 
                                // we add a procedure because CallRequest.fromCallRequestDTO requires a procedure on incoming RPC's
                                procedure: '__internal-identity__' }), { trace: { caller: 'WebSocketServer' } })))), () => ws.close());
                            return;
                        }
                        if (incomingMessage.identity) {
                            if ((_f = (_e = (_d = this.options.config) === null || _d === void 0 ? void 0 : _d.server) === null || _e === void 0 ? void 0 : _e.websocket) === null || _f === void 0 ? void 0 : _f.onClientConnectionIdentityChanged) {
                                if (!isEqual(incomingMessage.identity, identity)) {
                                    this.options.config.server.websocket.onClientConnectionIdentityChanged({
                                        connectionId: websocketId,
                                        identity: incomingMessage.identity,
                                        ip: wsIp,
                                    });
                                }
                            }
                        }
                        identity = incomingMessage.identity;
                    }
                    if ('clientMessage' in incomingMessage) {
                        this.clientMessageHandlers.forEach((msgHandler) => {
                            try {
                                msgHandler(Object.assign(Object.assign({}, incomingMessage), { connectionId: websocketId, identity }));
                            }
                            catch (err) {
                                console.log('error calling clientMessageHandler() with incoming websocket client msg', err);
                            }
                        });
                        return;
                    }
                    const requestDTO = incomingMessage;
                    // This is just a "set my identity" msg from the client
                    if (!requestDTO.procedure && requestDTO.identity) {
                        try {
                            const response = new CallResponse({
                                code: 200,
                                correlationId: requestDTO.correlationId,
                                success: true,
                            }, CallRequest.fromCallRequestDTO(Object.assign(Object.assign({}, requestDTO), { 
                                // we add a procedure because CallRequest.fromCallRequestDTO requires a procedure on incoming RPC's
                                procedure: '__internal-set-identity__' }), { trace: { caller: 'WebSocketServer' } }));
                            ws.send(JSON.stringify(CallResponse.toCallResponseDTO(response)));
                        }
                        catch (e) {
                            console.log('error sending CallResponseDTO to WS connection after setting RPCClientIdentity, error:', e);
                        }
                        return;
                    }
                    let request;
                    try {
                        request = CallRequest.fromCallRequestDTO(Object.assign({ identity }, requestDTO), { trace: { caller: 'WebSocketServer', ipAddress: wsIp } });
                    }
                    catch (e) {
                        debug('WebSocketServer error: WS msg not a valid CallRequestDTO', request, e);
                        return;
                    }
                    if (request) {
                        debug('handling received request');
                        if (request.identity) {
                            if (!isIdentityValid(request.identity)) {
                                console.log('error recieving CallResponseDTO with invalid RPCClientIdentity schema', request.identity);
                                return;
                            }
                            identity = request.identity;
                        }
                        const response = yield this.handleIncomingRequest(request);
                        debug('returning request response');
                        try {
                            ws.send(JSON.stringify(CallResponse.toCallResponseDTO(response)));
                        }
                        catch (e) {
                            console.log('WebSocketServer error: error sending CallResponseDTO to WS connection', response, e);
                        }
                        return;
                    }
                }));
            });
            this.wss.once('listening', () => {
                debug(`${this.options.config.displayName} listening ${this.host}:${this.port}`);
                if (onStartedCallback) {
                    onStartedCallback();
                }
            });
        };
        this.stop = (onStoppedCallback) => {
            var _a, _b, _c;
            (_a = this.wss) === null || _a === void 0 ? void 0 : _a.close((err) => {
                if (err) {
                    console.log('WSS error onclose:', err);
                }
                if (onStoppedCallback)
                    onStoppedCallback();
            });
            for (const ws of (_c = (_b = this.wss) === null || _b === void 0 ? void 0 : _b.clients) !== null && _c !== void 0 ? _c : []) {
                ws.terminate();
                ws.close();
            }
        };
        this.handleIncomingRequest = (request) => __awaiter(this, void 0, void 0, function* () {
            try {
                debug('sending request to requestHandler');
                return yield this.requestHandler(request);
            }
            catch (e) {
                if (e instanceof CallResponse) {
                    debug('handleIncomingRequest catching a throw CallResponse');
                    return e;
                }
                console.log('WebSocketServer error: there was an error handling request:', request, e);
                return new CallResponse({
                    code: 500,
                    message: 'There was a problem calling the procedure',
                    success: false,
                }, request);
            }
        });
        if (!((_b = (_a = options.config.server) === null || _a === void 0 ? void 0 : _a.websocket) === null || _b === void 0 ? void 0 : _b.port)) {
            throw new Error('config.gatewayServer.websocket.port is required');
        }
        this.host = ((_d = (_c = options.config.server) === null || _c === void 0 ? void 0 : _c.websocket) === null || _d === void 0 ? void 0 : _d.host) || DEFAULT_BIND;
        this.port = (_f = (_e = options.config.server) === null || _e === void 0 ? void 0 : _e.websocket) === null || _f === void 0 ? void 0 : _f.port;
        if (options.config.server.websocket.onClientMessage) {
            this.clientMessageHandlers.push(options.config.server.websocket.onClientMessage);
        }
    }
    static parseIPAddressFromWsReq(req) {
        let ip = '';
        if (req.headers['x-real-ip']) {
            if (Array.isArray(req.headers['x-real-ip'])) {
                ip = req.headers['x-real-ip'][0];
            }
            else {
                ip = req.headers['x-real-ip'].split(',')[0];
            }
        }
        else {
            ip = req.socket.remoteAddress || '';
        }
        ip = ip.trim();
        return ip;
    }
}
//# sourceMappingURL=WebSocketServer.js.map