var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { TelemetryManager } from './TelemetryManager';
describe('TelemetryInfoManager', () => {
    const rpcsServerOptions = {
        config: {
            displayName: 'test',
            ephemeralId: Math.random().toString(),
            telemetry: {
                adapter: {
                    mongoURI: 'mongodb://localhost:27017/test',
                },
            },
        },
    };
    // it('tracks stats', () => {
    //   const telemetryInfoManager = new TelemetryInfoManager(rpcsServerOptions)
    //
    //   eventBus.publish({
    //     topic: RPCEventTopics.handler_call_success,
    //     payload: makeHandlerCallSuccessPayload({
    //       response: CallResponse.EMPTY,
    //       request: CallRequest.EMPTY,
    //     }),
    //   })
    //
    //   eventBus.publish({
    //     topic: RPCEventTopics.handler_error,
    //     payload: makeHandlerErrorPayload({
    //       error: new Error('some error'),
    //       response: CallResponse.EMPTY,
    //       request: CallRequest.EMPTY,
    //     }),
    //   })
    //
    //   eventBus.publish({
    //     topic: RPCEventTopics.rpc_server_error,
    //     payload: makeRPCServerErrorPayload({
    //       error: new Error(''),
    //       request: CallRequest.EMPTY,
    //     }),
    //   })
    //
    //   expect(telemetryInfoManager.getStatistics().handlerErrorCount).toEqual(1)
    //   expect(telemetryInfoManager.getStatistics().handlerSuccessCount).toEqual(1)
    //   expect(telemetryInfoManager.getStatistics().rpcServerErrorCount).toEqual(1)
    // })
    it('saves procedure descriptions to mongo', () => __awaiter(void 0, void 0, void 0, function* () {
        const telemetryInfoManager = new TelemetryManager(rpcsServerOptions);
        telemetryInfoManager.registerHandler({
            procedure: 'test',
            scope: 'test',
            version: '1',
        });
        try {
            telemetryInfoManager.startTelemetryReporting();
        }
        catch (e) {
            // will throw
            // todo: need to implement mongodb mock for test env
        }
        yield new Promise((r) => setTimeout(r, 1000));
    }));
});
//# sourceMappingURL=TelemetryInfoManager.test.js.map