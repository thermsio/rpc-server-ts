var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import os from 'os';
import { TelemetryDataManager } from '../../TelemetryServer/intrinsic/TelemetryDataManager';
export class TelemetryInfoManager {
    constructor(options) {
        var _a, _b, _c, _d;
        this.options = options;
        this.heartbeatData = {
            hostName: os.hostname(),
            ephemeralId: '',
            gatewayHttpServer: false,
            gatewayWebSocketServer: false,
            displayName: '',
            startTime: '',
        };
        this.registeredHandlers = [];
        this.getHeartbeatData = () => this.heartbeatData;
        this.getRegisteredHandlers = () => this.registeredHandlers;
        this.registerHandler = (request) => {
            this.registeredHandlers.push(request);
        };
        this.startTelemetryReporting = () => __awaiter(this, void 0, void 0, function* () {
            this.heartbeatInterval = setInterval(() => {
                this.saveHeartbeat();
            }, 30000);
            yield this.saveHeartbeat();
            yield this.telemetryDataManager.saveHandlersRegistrationInfo(this.registeredHandlers.map((request) => (Object.assign(Object.assign({}, request), { createdAt: new Date().toISOString(), serverDisplayName: this.options.config.displayName }))));
        });
        this.saveHeartbeat = () => __awaiter(this, void 0, void 0, function* () {
            return this.telemetryDataManager.saveHeartbeat(this.heartbeatData);
        });
        if (!((_a = options.config.telemetry) === null || _a === void 0 ? void 0 : _a.adapter)) {
            throw new Error('config.telemetry.adapter must be set');
        }
        this.telemetryDataManager = new TelemetryDataManager((_b = options.config.telemetry) === null || _b === void 0 ? void 0 : _b.adapter);
        this.heartbeatData.ephemeralId = options.config.ephemeralId;
        this.heartbeatData.gatewayHttpServer = !!((_c = options.config.server) === null || _c === void 0 ? void 0 : _c.http);
        this.heartbeatData.gatewayWebSocketServer =
            !!((_d = options.config.server) === null || _d === void 0 ? void 0 : _d.websocket);
        this.heartbeatData.displayName = options.config.displayName;
        this.heartbeatData.startTime = new Date().toISOString();
    }
}
//# sourceMappingURL=TelemetryInfoManager.js.map