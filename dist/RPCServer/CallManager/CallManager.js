var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { CallRequest } from '../../CallRequest';
import { CallResponse } from '../../CallResponse';
import { LocalHandlerManager } from './intrinsic/LocalHandlerManager';
import { MessageBrokerManager } from './intrinsic/MessageBroker/MessageBrokerManager';
import Debug from 'debug';
import { makeHandlerRequestKey } from './intrinsic/utils/make-handler-request-key';
const debug = Debug('rpc:CallManager');
export class CallManager {
    constructor(options) {
        var _a;
        this.options = options;
        this.getRegisteredHandlers = () => this.localHandlerManager.getRegisteredHandlers();
        this.manageRequest = (request) => __awaiter(this, void 0, void 0, function* () {
            var _b, _c, _d, _e;
            const startTime = Date.now();
            const localHandler = this.localHandlerManager.checkCanHandleRequest(CallRequest.toCallRequestDescription(request));
            debug('manageRequest', request);
            const startRequestValue = (_c = (_b = this.options.config.handlers) === null || _b === void 0 ? void 0 : _b.onHandlerStartRequest) === null || _c === void 0 ? void 0 : _c.call(_b, request);
            if (localHandler) {
                debug('manageRequest, local handler found');
                return this.localHandlerManager.manageRequest(request).finally(() => {
                    var _a, _b;
                    (_b = (_a = this.options.config.handlers) === null || _a === void 0 ? void 0 : _a.onHandlerEndRequest) === null || _b === void 0 ? void 0 : _b.call(_a, request, startRequestValue);
                });
            }
            if (this.messageBrokerManager) {
                debug('manageRequest, sending to MessageBrokerManager');
                return this.messageBrokerManager.manageRequest(request).finally(() => {
                    var _a, _b;
                    (_b = (_a = this.options.config.handlers) === null || _a === void 0 ? void 0 : _a.onHandlerEndRequest) === null || _b === void 0 ? void 0 : _b.call(_a, request, startRequestValue);
                });
            }
            else {
                debug(`no messageBrokerManager found to proxy request, returning 501 no handler found (not implemented) for ${makeHandlerRequestKey(request)}`);
                (_e = (_d = this.options.config.handlers) === null || _d === void 0 ? void 0 : _d.onHandlerEndRequest) === null || _e === void 0 ? void 0 : _e.call(_d, request, startRequestValue);
                return new CallResponse({
                    code: 501,
                    message: `No procedure handler found for scope=${request.scope}, procedure=${request.procedure}, version=${request.version}. Trace info: caller=${request.trace.caller} internal=${!!request
                        .trace.internal}`,
                    success: false,
                }, request);
            }
        });
        this.registerHandler = (request, handler) => {
            this.localHandlerManager.registerHandler(request, handler);
            if (this.messageBrokerManager) {
                this.messageBrokerManager.registerHandler(request);
            }
        };
        this.localHandlerManager = new LocalHandlerManager({
            config: options.config,
        });
        if ((_a = options.config) === null || _a === void 0 ? void 0 : _a.messageBroker) {
            this.messageBrokerManager = new MessageBrokerManager({
                config: options.config,
                requestHandler: this.manageRequest,
            });
        }
    }
}
//# sourceMappingURL=CallManager.js.map