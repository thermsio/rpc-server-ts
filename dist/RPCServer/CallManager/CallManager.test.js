var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { CallManager } from './CallManager';
import { CallResponse } from '../../CallResponse';
import { CallRequest } from '../../CallRequest';
describe('CallManager', () => {
    const rpcsServerOptions = {
        config: {
            displayName: 'test',
            ephemeralId: Math.random().toString(),
        },
    };
    it('can instantiate', () => {
        new CallManager(rpcsServerOptions);
    });
    it('can register request handler', () => {
        const rpcsServer = new CallManager(rpcsServerOptions);
        const handler = (request) => Promise.resolve(CallResponse.EMPTY);
        rpcsServer.registerHandler({ procedure: 'call-mgr', scope: 'call-manager', version: '1' }, handler);
    });
    it('can manage a request with local handler', () => __awaiter(void 0, void 0, void 0, function* () {
        const rpcsServer = new CallManager(rpcsServerOptions);
        const responseObj = {
            code: 200,
            data: [],
            message: 'good',
            success: true,
        };
        const handler = (request) => {
            return Promise.resolve(new CallResponse(responseObj, request));
        };
        const request = {
            procedure: 'call-mgr',
            scope: 'call-manager',
            version: '1',
        };
        rpcsServer.registerHandler(request, handler);
        const response = yield rpcsServer.manageRequest(CallRequest.fromCallRequestDTO(request, { trace: { caller: 'test' } }));
        const responseDTO = CallResponse.toCallResponseDTO(response);
        expect(responseDTO).toEqual({
            code: 200,
            data: [],
            message: 'good',
            success: true,
        });
    }));
    it('catches a thrown CallResponse', () => __awaiter(void 0, void 0, void 0, function* () {
        const rpcsServer = new CallManager(rpcsServerOptions);
        const handler = (request) => __awaiter(void 0, void 0, void 0, function* () {
            throw new CallResponse({ code: 400, success: false }, request);
            return Promise.resolve(new CallResponse({ code: 200, success: true }, request));
        });
        const request = {
            procedure: 'call-mgr',
            scope: 'call-manager',
            version: '1',
        };
        rpcsServer.registerHandler(request, handler);
        const response = yield rpcsServer.manageRequest(CallRequest.fromCallRequestDTO(request, { trace: { caller: 'test' } }));
        const responseDTO = CallResponse.toCallResponseDTO(response);
        expect(responseDTO).toEqual({
            code: 400,
            success: false,
        });
    }));
    it('catches a throws and will make a valid response', () => __awaiter(void 0, void 0, void 0, function* () {
        const rpcsServer = new CallManager(rpcsServerOptions);
        // a custom class that will be thrown
        class RPCHandlerError {
            constructor(response) {
                this.success = false;
                this.code = response.code || 500;
                this.message = response.message || 'The was a RPC handler error';
            }
        }
        const handler = (request) => __awaiter(void 0, void 0, void 0, function* () {
            throw new RPCHandlerError({ code: 400, message: 'thrown thing' });
            return Promise.resolve(new CallResponse({ code: 200, success: true }, request));
        });
        const request = {
            procedure: 'call-mgr-throw',
            scope: 'call-manager',
            version: '1',
        };
        rpcsServer.registerHandler(request, handler);
        const response = yield rpcsServer.manageRequest(CallRequest.fromCallRequestDTO(request, { trace: { caller: 'test' } }));
        const responseDTO = CallResponse.toCallResponseDTO(response);
        expect(responseDTO.code).toEqual(400);
        expect(responseDTO.message).toEqual('thrown thing');
        expect(responseDTO.success).toEqual(false);
    }));
    it('prevents non-internal calls from getting to internal handlers', () => __awaiter(void 0, void 0, void 0, function* () {
        const rpcsServer = new CallManager(rpcsServerOptions);
        const handler = (request) => __awaiter(void 0, void 0, void 0, function* () {
            return Promise.resolve(new CallResponse({ code: 200, success: true }, request));
        });
        const internalRequest = {
            procedure: 'call-mgr-internal',
            scope: 'call-manager',
            version: '1',
        };
        const publicRequest = {
            procedure: 'call-mgr-public',
            scope: 'call-manager',
            version: '1',
        };
        rpcsServer.registerHandler(Object.assign({ internal: true }, internalRequest), handler);
        rpcsServer.registerHandler(publicRequest, handler);
        const internalResponse = yield rpcsServer.manageRequest(CallRequest.fromCallRequestDTO(internalRequest, {
            trace: { caller: 'test' },
        }));
        const publicResponse = yield rpcsServer.manageRequest(CallRequest.fromCallRequestDTO(publicRequest, {
            trace: { caller: 'test' },
        }));
        const internalResponseDTO = CallResponse.toCallResponseDTO(internalResponse);
        const publicResponseDTO = CallResponse.toCallResponseDTO(publicResponse);
        expect(internalResponseDTO).toMatchObject({
            code: 403,
            success: false,
        });
        expect(publicResponseDTO).toMatchObject({
            code: 200,
            success: true,
        });
    }));
    it('calls start/end lifecycles for handlers', (done) => {
        const requestInfo = {
            procedure: 'call-mgr-tracing-handlers',
            scope: 'call-manager',
            version: '1',
        };
        const callMgr = new CallManager(Object.assign(Object.assign({}, rpcsServerOptions), { config: Object.assign(Object.assign({}, rpcsServerOptions.config), { handlers: {
                    onHandlerEndRequest: (request, startValue) => {
                        expect(request).toMatchObject(requestInfo);
                        expect(startValue).toEqual('trace_id');
                        done();
                    },
                    onHandlerStartRequest: (request) => {
                        expect(request).toMatchObject(requestInfo);
                        return 'trace_id';
                    },
                } }) }));
        callMgr.registerHandler(requestInfo, (request) => {
            return Promise.resolve(new CallResponse({ code: 200, success: true }, request));
        });
        callMgr.manageRequest(CallRequest.fromCallRequestDTO(requestInfo, {
            trace: { caller: 'test' },
        }));
    });
});
//# sourceMappingURL=CallManager.test.js.map