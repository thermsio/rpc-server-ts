var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { MessageBrokerManager } from './MessageBrokerManager';
import { CallResponse } from '../../../../CallResponse';
describe('MessageBrokerManager', () => {
    it('can instantiate', () => {
        const messageBrokerManager = new MessageBrokerManager({
            config: {
                displayName: 'test',
                ephemeralId: Math.random().toString(),
                messageBroker: {
                    amqpURI: 'amqp://localhost:5672',
                },
            },
            requestHandler: () => __awaiter(void 0, void 0, void 0, function* () { return Promise.resolve(CallResponse.EMPTY); }),
        });
        expect(messageBrokerManager).toBeDefined();
    });
});
//# sourceMappingURL=MessageBrokerManager.test.js.map