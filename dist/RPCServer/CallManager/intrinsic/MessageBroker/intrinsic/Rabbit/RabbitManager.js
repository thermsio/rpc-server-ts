var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import amqp from 'amqp-connection-manager';
import Debug from 'debug';
import { RabbitRpc } from './intrinsic/RabbitRpc';
const debug = Debug('rpc:RabbitManager');
const error = (...args) => console.log('RabbitManager Err:', ...args);
export class RabbitManager {
    constructor(options) {
        var _a, _b;
        this.options = options;
        this.connected = false;
        this.sendRequestForResponse = (request) => {
            return this.rpc.sendRequestForResponse(request);
        };
        this.shutdown = () => __awaiter(this, void 0, void 0, function* () {
            // cannot restart once this has been called
            try {
                yield this.rpc.shutdown();
                yield this.connection.close();
            }
            finally {
            }
        });
        this.subscribeToHandleRequest = (request) => {
            return this.rpc.subscribeToHandleRequest(request);
        };
        options.requestHandler;
        this.uniqueQueueName = `${options.config.displayName}-${options.config.ephemeralId}`;
        if (!((_b = (_a = options.config) === null || _a === void 0 ? void 0 : _a.messageBroker) === null || _b === void 0 ? void 0 : _b.amqpURI)) {
            throw new Error('amqpURI is required');
        }
        this.connection = amqp.connect([options.config.messageBroker.amqpURI], {
            // credentials: {
            //   username: '',
            //   password: ''
            // },
            heartbeatIntervalInSeconds: 5,
        });
        this.connection.on('connect', ({ url }) => {
            this.connected = true;
            debug('rabbit connected to: ', url);
        });
        this.connection.on('disconnect', ({ err }) => {
            if (this.connected) {
                error('disconnected from rabbit broker');
            }
            else {
                error('failed to connect to rabbit broker');
            }
        });
        this.rpc = new RabbitRpc({
            config: options.config,
            connection: this.connection,
            requestHandler: options.requestHandler,
        });
    }
}
//# sourceMappingURL=RabbitManager.js.map