var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
import { RabbitManager } from './RabbitManager';
import { CallResponse } from '../../../../../../CallResponse';
import { CallRequest } from '../../../../../../CallRequest';
import { LocalHandlerManager } from '../../../LocalHandlerManager';
describe('RabbitManager', () => {
    const config = {
        displayName: 'test',
        ephemeralId: 'test',
    };
    const localHandlerManager = new LocalHandlerManager({
        config,
    });
    const rpcServerOptions = {
        config: {
            displayName: 'test',
            ephemeralId: Math.random().toString(),
            messageBroker: {
                amqpURI: 'amqp://127.0.0.1:5672',
            },
        },
        requestHandler: localHandlerManager.manageRequest,
    };
    it('can instantiate', (done) => {
        const rabbit = new RabbitManager(rpcServerOptions);
        rabbit.shutdown().then(done);
    });
    it('can add a request handler and respond to queue RPCs', () => __awaiter(void 0, void 0, void 0, function* () {
        const rabbit = new RabbitManager(rpcServerOptions);
        const handler = (request) => __awaiter(void 0, void 0, void 0, function* () {
            expect(request.procedure).toEqual('test-single');
            expect(request.scope).toEqual('test-single-scope');
            expect(request.version).toEqual('1');
            expect(request.trace.id).toEqual('abc123abc123');
            const response = new CallResponse({
                code: 200,
                data: 'ABC123',
                success: true,
            }, request);
            return response;
        });
        const requestDescription = {
            procedure: 'test-single',
            scope: 'test-single-scope',
            version: '1',
        };
        localHandlerManager.registerHandler(requestDescription, handler);
        yield rabbit.subscribeToHandleRequest(requestDescription);
        const request = new CallRequest({
            procedure: 'test-single',
            scope: 'test-single-scope',
            trace: { caller: 'test', id: 'abc123abc123' },
            version: '1',
        });
        const response = yield rabbit.sendRequestForResponse(request);
        expect(response.data).toBeDefined();
        expect(response.data).toEqual('ABC123');
        yield rabbit.shutdown();
    }));
    it('can respond to multiple queue RPCs', () => __awaiter(void 0, void 0, void 0, function* () {
        var _a, e_1, _b, _c;
        const localHandlerManager = new LocalHandlerManager({
            config,
        });
        const rpcServerOptions = {
            config: {
                displayName: 'test',
                ephemeralId: Math.random().toString(),
                messageBroker: {
                    amqpURI: 'amqp://127.0.0.1:5672',
                },
            },
            requestHandler: localHandlerManager.manageRequest,
        };
        const rabbit = new RabbitManager(rpcServerOptions);
        const handler = (request) => __awaiter(void 0, void 0, void 0, function* () {
            return new CallResponse({
                code: 200,
                data: `data-${request.args}`,
                success: true,
            }, request);
        });
        const requestDescription = {
            procedure: 'test',
            scope: 'test',
            version: '1',
        };
        localHandlerManager.registerHandler(requestDescription, handler);
        yield rabbit.subscribeToHandleRequest(requestDescription);
        const countList = new Array(10).fill(null);
        let index = 0;
        try {
            for (var _d = true, countList_1 = __asyncValues(countList), countList_1_1; countList_1_1 = yield countList_1.next(), _a = countList_1_1.done, !_a; _d = true) {
                _c = countList_1_1.value;
                _d = false;
                const _ = _c;
                const response = yield rabbit.sendRequestForResponse(new CallRequest({
                    args: index,
                    procedure: 'test',
                    scope: 'test',
                    trace: { caller: 'test', id: `id-${index}` },
                    version: '1',
                }));
                expect(response.data).toEqual(`data-${index}`);
                index++;
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (!_d && !_a && (_b = countList_1.return)) yield _b.call(countList_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        yield rabbit.shutdown();
    }));
    it('will respond when no handler is registered for a scope/procedure/version', () => __awaiter(void 0, void 0, void 0, function* () {
        const rabbit = new RabbitManager(rpcServerOptions);
        const requestDescription = {
            procedure: 'no-handler-expected',
            scope: '',
            version: '1',
        };
        yield rabbit.subscribeToHandleRequest(requestDescription);
        const request = new CallRequest(Object.assign(Object.assign({}, requestDescription), { trace: { caller: 'test', id: '098776655' } }));
        const response = yield rabbit.sendRequestForResponse(request);
        expect(response.code).toEqual(501);
        expect(response.data).toBeUndefined();
        expect(response.success).toBeFalsy();
        yield rabbit.shutdown();
    }));
});
//# sourceMappingURL=RabbitManager.test.js.map