var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import Debug from 'debug';
import { PromiseTimeoutError, PromiseWrapper, } from '../../../../../../../utils/PromiseWrapper';
import { CallResponse } from '../../../../../../../CallResponse';
import ObjectId from 'bson-objectid';
import { CallRequest, DEFAULT_REQUEST_SCOPE, } from '../../../../../../../CallRequest';
import { makeHandlerRequestKey } from '../../../../utils/make-handler-request-key';
const RPC_GLOBAL_EXCHANGE_NAME = 'rpc-global'; // for requests that have no "scope"
const RPC_SCOPED_EXCHANGE_NAME = 'rpc-scoped'; // for requests that have a "scope"
const debug = Debug('rpc:RabbitRpc');
var QueueMessageTypes;
(function (QueueMessageTypes) {
    QueueMessageTypes["Request"] = "request";
    QueueMessageTypes["Response"] = "response";
})(QueueMessageTypes || (QueueMessageTypes = {}));
export class RabbitRpc {
    constructor(options) {
        this.options = options;
        this.pendingPromisesForResponse = {};
        this.sendRequestForResponse = (request) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            debug('sendRequestForResponse()');
            // await this.channelWrapper.waitForConnect()
            const promiseWrapper = new PromiseWrapper();
            this.pendingPromisesForResponse[request.trace.id] = promiseWrapper;
            const exchange = request.scope === DEFAULT_REQUEST_SCOPE
                ? RPC_GLOBAL_EXCHANGE_NAME
                : RPC_SCOPED_EXCHANGE_NAME;
            yield this.channelWrapper.publish(exchange, this.makeExchangeRoutingKey(CallRequest.toCallRequestDescription(request)), this.makeQueueRPCObject({ request }), {
                appId: this.options.config.displayName,
                correlationId: (_a = request === null || request === void 0 ? void 0 : request.trace) === null || _a === void 0 ? void 0 : _a.id,
                mandatory: true, // rabbit will return if cannot be routed, this.handleReturnedQueueMsg
                replyTo: this.uniqueQueueName,
                type: QueueMessageTypes.Request,
            });
            try {
                return yield promiseWrapper.promise;
            }
            catch (e) {
                let message = '';
                if (e instanceof PromiseTimeoutError) {
                    message = 'no response received (timed out)';
                }
                else {
                    message = 'there was an error processing RPC';
                }
                return new CallResponse({
                    code: 500,
                    message,
                    success: false,
                }, request);
            }
        });
        this.shutdown = () => this.channelWrapper.close();
        this.subscribeToHandleRequest = (request) => __awaiter(this, void 0, void 0, function* () {
            var _b;
            debug('subscribeToHandleRequest', request);
            if (!this.channelWrapper) {
                throw new Error('cannot subscribe request handler because rabbitmq was not initialized properly');
            }
            (_b = this.channelWrapper) === null || _b === void 0 ? void 0 : _b.addSetup((channel) => __awaiter(this, void 0, void 0, function* () {
                const exchange = request.scope === DEFAULT_REQUEST_SCOPE
                    ? RPC_GLOBAL_EXCHANGE_NAME
                    : RPC_SCOPED_EXCHANGE_NAME;
                yield channel.bindQueue(this.uniqueQueueName, exchange, this.makeExchangeRoutingKey(request));
            }));
        });
        this.error = (...args) => console.log('RabbitRpc ERROR', ...args);
        this.handleReturnedQueueMsg = (msg) => __awaiter(this, void 0, void 0, function* () {
            debug('handleReturnedQueueMsg()');
            const { request } = this.parseQueueMsgContent(msg);
            if (!request) {
                this.error('rcvd returned queue msg and cannot handle', msg);
                return;
            }
            if (this.pendingPromisesForResponse[request.trace.id]) {
                this.pendingPromisesForResponse[request.trace.id].resolve(new CallResponse({
                    code: 501,
                    message: 'no registered handlers for RPC',
                    success: false,
                }, request));
            }
            else {
                debug('a queue message was returned but no pending requests found', request, msg);
            }
        });
        this.handleRequestFromQueue = (msg) => __awaiter(this, void 0, void 0, function* () {
            debug('handleRequestFromQueue()');
            const { properties } = msg;
            const { request } = this.parseQueueMsgContent(msg);
            if (!request) {
                this.error('rcvd queue msg that is not a valid RPC request', msg);
                return;
            }
            let response;
            try {
                response = yield this.requestHandler(request);
                if (!response) {
                    throw new Error('request handler provided no response');
                }
            }
            catch (e) {
                this.error('error handling request from the queue', e);
                this.error('   request: ', request);
            }
            try {
                if (response) {
                    yield this.sendHandledResponseToQueue(properties.replyTo, response, request);
                }
            }
            catch (e) {
                this.error('error sending response to the queue', e);
                this.error('   response: ', response);
            }
        });
        this.handleResponseFromQueue = (msg) => __awaiter(this, void 0, void 0, function* () {
            debug('handleResponseFromQueue()');
            const { properties } = msg;
            const { response, request } = this.parseQueueMsgContent(msg);
            if (!request || !response) {
                this.error('rcvd queue msg that is not a valid RPC response', msg);
                return;
            }
            if (this.pendingPromisesForResponse[response.trace.id]) {
                this.pendingPromisesForResponse[response.trace.id].resolve(response);
            }
            else {
                this.error('received queue msg response with no pending request found locally', request, response);
            }
        });
        this.makeExchangeRoutingKey = (request) => makeHandlerRequestKey(request);
        /**
         * This method creates the structure for objects that we send over the queue
         * @param request {CallRequest}
         * @param response {CallResponse}
         */
        this.makeQueueRPCObject = ({ request, response, }) => ({
            request,
            response,
        });
        this.onReturnedQueueMsg = (msg) => {
            debug('onReturnedQueueMsg()');
            this.handleReturnedQueueMsg(msg);
        };
        this.onMsgReceivedFromQueue = (msg) => __awaiter(this, void 0, void 0, function* () {
            debug('handleMsgFromQueue() message', msg);
            if (!msg) {
                debug('queue consumer received empty msg');
                return;
            }
            switch (msg.properties.type) {
                case QueueMessageTypes.Response:
                    this.handleResponseFromQueue(msg);
                    break;
                case QueueMessageTypes.Request:
                    this.handleRequestFromQueue(msg);
                    break;
                default:
                    this.error('rcvd rabbit queue msg with unrecognized "type" property: ', msg);
            }
        });
        this.parseQueueMsgContent = (msg) => {
            try {
                let json;
                json = JSON.parse(msg.content.toString());
                if (!json.request) {
                    debug('parseQueueMsg() json has no "request" prop', json);
                    return {};
                }
                const response = json.response
                    ? new CallResponse(json.response, json.request)
                    : undefined;
                const request = new CallRequest(json.request);
                return {
                    response,
                    request,
                };
            }
            catch (e) {
                this.error('unable to parse queue msg content', e, msg);
                return {};
            }
        };
        this.sendHandledResponseToQueue = (queueName, response, request) => __awaiter(this, void 0, void 0, function* () {
            var _c;
            debug('sendHandledResponseToQueue() sending response for a request', request);
            yield this.channelWrapper.sendToQueue(queueName, this.makeQueueRPCObject({ request, response }), {
                appId: this.options.config.displayName,
                correlationId: (_c = request === null || request === void 0 ? void 0 : request.trace) === null || _c === void 0 ? void 0 : _c.id,
                type: QueueMessageTypes.Response,
            });
        });
        this.uniqueQueueName = `${options.config.displayName}-rpc-${new ObjectId().toString()}`;
        this.requestHandler = options.requestHandler;
        this.channelWrapper = options.connection.createChannel({
            json: true,
            setup: (channel) => {
                // @ts-ignore
                channel.addListener('return', this.onReturnedQueueMsg);
                return Promise.all([
                    channel.assertExchange(RPC_GLOBAL_EXCHANGE_NAME, 'direct'),
                    channel.assertExchange(RPC_SCOPED_EXCHANGE_NAME, 'direct'),
                    channel.assertQueue(this.uniqueQueueName, {
                        autoDelete: true,
                        durable: false,
                        // deadLetterExchange?: string; // todo
                        // deadLetterRoutingKey?: string;
                        // maxPriority?: number; // todo
                    }),
                    channel.consume(this.uniqueQueueName, this.onMsgReceivedFromQueue, {
                        noAck: true,
                    }),
                ]);
            },
        });
    }
}
//# sourceMappingURL=RabbitRpc.js.map