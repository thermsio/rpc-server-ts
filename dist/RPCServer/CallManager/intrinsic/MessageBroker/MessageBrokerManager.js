var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { RabbitManager } from './intrinsic/Rabbit/RabbitManager';
/**
 * The MessageBrokerManager is responsible for managing the messageBroker in the
 * server (rabbitmq, kafka, redis, etc.).
 */
export class MessageBrokerManager {
    constructor(options) {
        var _a;
        this.options = options;
        this.manageRequest = (request) => __awaiter(this, void 0, void 0, function* () {
            return this.messageQueue.sendRequestForResponse(request);
        });
        this.registerHandler = (request) => __awaiter(this, void 0, void 0, function* () {
            return this.messageQueue.subscribeToHandleRequest(request);
        });
        if (!((_a = options.config.messageBroker) === null || _a === void 0 ? void 0 : _a.amqpURI))
            throw new Error('cannot be initialized without config.messageBroker.amqpURI connection info for a MessageBroker');
        this.messageQueue = new RabbitManager(options);
    }
}
//# sourceMappingURL=MessageBrokerManager.js.map