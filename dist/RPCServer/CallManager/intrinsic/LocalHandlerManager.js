var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import Ajv from 'ajv';
import { CallRequest } from '../../../CallRequest';
import { CallResponse } from '../../../CallResponse';
import { makeHandlerRequestKey } from './utils/make-handler-request-key';
import Debug from 'debug';
const ajv = new Ajv({
    coerceTypes: 'array',
    removeAdditional: true,
    useDefaults: true, // ie: convert { name: null } to { name: "" } if the the name=string type
});
const debug = Debug('rpc:LocalHandlerManager');
export class LocalHandlerManager {
    constructor(options) {
        this.options = options;
        this.handlersByHandlerRequestKey = {};
        this.handlerRequestValidationsByHandlerRequestKey = {};
        this.checkCanHandleRequest = (request) => {
            return !!this.handlersByHandlerRequestKey[makeHandlerRequestKey(request)];
        };
        this.getRegisteredHandlers = () => {
            return Object.keys(this.handlersByHandlerRequestKey);
        };
        this.getSimilarRegisteredProcedures = (request) => {
            const rgx = new RegExp(request.procedure);
            return Object.keys(this.handlersByHandlerRequestKey)
                .filter((key) => {
                return rgx.test(key);
            })
                .join(', ');
        };
        this.manageRequest = (request) => __awaiter(this, void 0, void 0, function* () {
            debug('manageRequest', request);
            try {
                const key = makeHandlerRequestKey(CallRequest.toCallRequestDescription(request));
                if (this.handlersByHandlerRequestKey[key].internalOnly &&
                    !request.trace.internal) {
                    debug('handler is marked "internalOnly" and the request is not coming from internal', request);
                    throw new CallResponse({
                        code: 403,
                        message: 'caller is not authorized to access this RPC',
                        success: false,
                    }, request);
                }
                if (this.handlerRequestValidationsByHandlerRequestKey[key]) {
                    this.handlerRequestValidationsByHandlerRequestKey[key](request);
                }
                return yield this.handlersByHandlerRequestKey[key].handler(request);
            }
            catch (e) {
                if (CallResponse.isCallResponse(e)) {
                    return e;
                }
                else if (CallResponse.isCallResponseDTO(e)) {
                    return new CallResponse(e, request);
                }
                console.log('LocalHandlerManager error: catching and returning 500', e);
                return new CallResponse({
                    code: 500,
                    success: false,
                }, request);
            }
        });
        this.registerHandler = (request, handler) => {
            var _a;
            debug('registerHandler', request);
            const key = makeHandlerRequestKey(request);
            this.handlersByHandlerRequestKey[key] = {
                internalOnly: !!request.internal,
                handler,
            };
            // Check if the HandlerRegistrationInfo has "args" as JSON-schema for automatic validation
            if (typeof request.args === 'object' && ((_a = request.args) === null || _a === void 0 ? void 0 : _a.type)) {
                try {
                    const validate = ajv.compile(request.args);
                    this.handlerRequestValidationsByHandlerRequestKey[key] = (request) => {
                        var _a;
                        const valid = validate(request.args || {});
                        if (!valid) {
                            debug('validation error', validate.errors);
                            throw new CallResponse({
                                code: 422,
                                message: `"args" validation failed: ${(_a = validate.errors) === null || _a === void 0 ? void 0 : _a.map((errorObj) => errorObj.instancePath + errorObj.message).join(', ')}`,
                                success: false,
                            }, request);
                        }
                        return undefined;
                    };
                }
                catch (e) {
                    console.log(``);
                    console.log(`ERROR: Unable to compile JSON-schema for "${key}"`);
                    console.log(`   ${e.message}`);
                    console.log(``);
                    // do not throw, exist the process, tests expect this impl
                    process.exit(1);
                }
            }
        };
    }
}
//# sourceMappingURL=LocalHandlerManager.js.map