export function makeHandlerRequestKey(request) {
    return `${request.scope}::${request.procedure}::${request.version}`;
}
//# sourceMappingURL=make-handler-request-key.js.map