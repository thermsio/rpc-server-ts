var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { LocalHandlerManager } from './LocalHandlerManager';
import { CallResponse } from '../../../CallResponse';
import { CallRequest } from '../../../CallRequest';
describe('LocalHandlerManager', () => {
    const config = {
        displayName: 'test',
        ephemeralId: 'test',
    };
    it('can instantiate', () => {
        const localHandlManager = new LocalHandlerManager({
            config,
        });
        expect(localHandlManager).toBeDefined();
    });
    it('can register a handler', () => {
        const localHandlManager = new LocalHandlerManager({
            config,
        });
        const requestRegInfo = {
            procedure: 'test',
            scope: 'local-handler-manager',
            version: '1',
        };
        localHandlManager.registerHandler(requestRegInfo, (request) => {
            return new CallResponse({ code: 200, success: true }, request);
        });
        expect(localHandlManager.checkCanHandleRequest(requestRegInfo)).toBeTruthy();
    });
    it('can manage a request', () => __awaiter(void 0, void 0, void 0, function* () {
        const localHandlManager = new LocalHandlerManager({
            config,
        });
        const requestRegInfo = {
            procedure: 'test',
            scope: 'local-handler-manager',
            version: '1',
        };
        localHandlManager.registerHandler(requestRegInfo, (request) => {
            return new CallResponse({ code: 200, success: true }, request);
        });
        try {
            const { success } = yield localHandlManager.manageRequest(CallRequest.fromCallRequestDTO(requestRegInfo, {
                trace: { caller: 'test' },
            }));
            expect(success).toBeTruthy();
        }
        catch (e) {
            expect(e).toBeUndefined();
        }
    }));
    it('will perform JSON-Schema validations when "args" are present', () => __awaiter(void 0, void 0, void 0, function* () {
        const localHandlManager = new LocalHandlerManager({
            config,
        });
        localHandlManager.registerHandler({
            args: {
                properties: {
                    name: { type: 'string' },
                },
                required: ['name'],
                type: 'object',
            },
            procedure: 'test',
            scope: 'local-handler-manager',
            version: '1',
        }, (request) => {
            return new CallResponse({ code: 200, success: true }, request);
        });
        try {
            const { success } = yield localHandlManager.manageRequest(CallRequest.fromCallRequestDTO({
                procedure: 'test',
                scope: 'local-handler-manager',
                version: '1',
            }, { trace: { caller: 'test' } }));
            expect(success).toBeFalsy();
        }
        catch (e) {
            expect(e).toBeUndefined();
        }
    }));
    it('will crash when invalid JSON-Schema "args" are passed to registration', () => __awaiter(void 0, void 0, void 0, function* () {
        const realProcess = process;
        const exitMock = jest.fn();
        // We assign all properties of the "real process" to
        // our "mock" process, otherwise, if "myFunc" relied
        // on any of such properties (i.e `process.env.NODE_ENV`)
        // it would crash with an error like:
        // `TypeError: Cannot read property 'NODE_ENV' of undefined`.
        // @ts-ignore
        global.process = Object.assign(Object.assign({}, realProcess), { exit: exitMock });
        const localHandlManager = new LocalHandlerManager({
            config,
        });
        try {
            localHandlManager.registerHandler({
                args: {
                    properties: {
                        name: { type: 'string' },
                    },
                    required: { 'this-should-be-an-array': '' },
                    type: 'not-a-real-property',
                },
                procedure: 'test',
                scope: 'local-handler-manager',
                version: '1',
            }, (request) => {
                return new CallResponse({ code: 200, success: true }, request);
            });
        }
        catch (e) {
            expect(exitMock).toHaveBeenCalledWith(1);
            global.process = realProcess;
        }
    }));
});
//# sourceMappingURL=LocalHandlerManager.test.js.map