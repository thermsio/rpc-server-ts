var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import axios from 'axios';
import { RPCServer } from './RPCServer';
import { CallResponse } from '../CallResponse';
describe('basic rpc gatewayServer', () => {
    it('can setup a basic http (http 1.1) gatewayServer and listen on a port', () => __awaiter(void 0, void 0, void 0, function* () {
        const server = new RPCServer({
            displayName: 'test',
            ephemeralId: Math.random().toString(),
            server: {
                http: { host: 'localhost', port: 6789 },
            },
        });
        server.registerHandler({ procedure: 'basic-gatewayServer-test', scope: 'global', version: '1' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
            return { code: 200, success: true };
        }));
        server.start();
        try {
            const { data, status } = yield axios.post('http://localhost:6789', {
                procedure: 'basic-gatewayServer-test',
            });
            expect(status).toEqual(200);
            expect(data).toMatchObject({ code: 200, success: true });
        }
        finally {
            server.stop();
            yield new Promise((r) => setTimeout(r, 1000));
        }
    }));
    it('handles scope correctly for procedures', () => __awaiter(void 0, void 0, void 0, function* () {
        const server = new RPCServer({
            displayName: 'test',
            ephemeralId: Math.random().toString(),
            server: {
                http: { host: 'localhost', port: 6789 },
            },
        });
        server.registerHandler({ procedure: 'scope-test', scope: 'global', version: '1' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
            return new CallResponse({
                code: 200,
                data: { scope: 'global' },
                success: true,
            }, request);
        }));
        server.registerHandler({ procedure: 'scope-test', scope: 'first', version: '1' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
            return new CallResponse({
                code: 200,
                data: { scope: 'first' },
                success: true,
            }, request);
        }));
        server.registerHandler({ procedure: 'scope-test', scope: 'second', version: '1' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
            return new CallResponse({
                code: 200,
                data: { scope: 'second' },
                success: true,
            }, request);
        }));
        server.start();
        try {
            const noScope = yield axios.post('http://localhost:6789', {
                procedure: 'scope-test',
            });
            expect(noScope.data.data.scope).toEqual('global');
            const first = yield axios.post('http://localhost:6789', {
                procedure: 'scope-test',
                scope: 'first',
            });
            expect(first.data.data.scope).toEqual('first');
            const second = yield axios.post('http://localhost:6789', {
                procedure: 'scope-test',
                scope: 'second',
            });
            expect(second.data.data.scope).toEqual('second');
        }
        finally {
            server.stop();
            yield new Promise((r) => setTimeout(r, 1000));
        }
    }));
    it('handles version correctly for procedures', () => __awaiter(void 0, void 0, void 0, function* () {
        const server = new RPCServer({
            displayName: 'test',
            ephemeralId: Math.random().toString(),
            server: {
                http: { host: 'localhost', port: 6789 },
            },
        });
        server.registerHandler({ procedure: 'version-test', scope: 'global', version: '1' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
            return new CallResponse({
                code: 200,
                data: { version: '1' },
                success: true,
            }, request);
        }));
        server.registerHandler({ procedure: 'version-test', scope: 'global', version: '2' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
            return new CallResponse({
                code: 200,
                data: { version: '2' },
                success: true,
            }, request);
        }));
        server.start();
        try {
            const noVersionSpecified = yield axios.post('http://localhost:6789', {
                procedure: 'version-test',
            });
            expect(noVersionSpecified.data.data.version).toEqual('1');
            const one = yield axios.post('http://localhost:6789', {
                procedure: 'version-test',
                version: '1',
            });
            expect(one.data.data.version).toEqual('1');
            const two = yield axios.post('http://localhost:6789', {
                procedure: 'version-test',
                version: '2',
            });
            expect(two.data.data.version).toEqual('2');
        }
        finally {
            server.stop();
            yield new Promise((r) => setTimeout(r, 1000));
        }
    }));
    it('provides a "call" method to make internal RPC calls', () => __awaiter(void 0, void 0, void 0, function* () {
        const server = new RPCServer({
            displayName: 'test',
            ephemeralId: Math.random().toString(),
        });
        server.registerHandler({ procedure: 'local', scope: 'global', version: '1' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
            if (request.args) {
                return {
                    code: 200,
                    data: { version: '1' },
                    success: true,
                };
            }
            else {
                return {
                    code: 422,
                    success: false,
                };
            }
        }));
        try {
            yield server.call({
                args: false,
                procedure: 'local',
                scope: 'global',
                version: '1',
            }, 'test');
        }
        catch (e) {
            expect(e.code).toEqual(422);
            expect(e.success).toBeFalsy();
        }
        const { code, success } = yield server.call({
            args: true,
            procedure: 'local',
            scope: 'global',
            version: '1',
        }, 'test');
        expect(code).toEqual(200);
        expect(success).toBeTruthy();
    }));
    it('registerHandler() provides a "call" method to the handler as 2nd param', () => __awaiter(void 0, void 0, void 0, function* () {
        const server = new RPCServer({
            displayName: 'test',
            ephemeralId: Math.random().toString(),
        });
        server.registerHandler({ procedure: 'local_1', scope: 'global', version: '1' }, (request, call) => __awaiter(void 0, void 0, void 0, function* () {
            var _a;
            expect((_a = request.identity) === null || _a === void 0 ? void 0 : _a.authorization).toEqual('123');
            // call another handler internally that returns the original request's identity
            const local2Response = yield call({
                procedure: 'local_2',
                scope: 'global',
                version: '1',
            });
            return {
                code: 200,
                data: local2Response.data,
                success: true,
            };
        }));
        server.registerHandler({ internal: true, procedure: 'local_2', scope: 'global', version: '1' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
            return ({
                code: 200,
                data: request.identity,
                success: true,
            });
        }));
        const { data } = yield server.call({
            args: true,
            identity: {
                authorization: '123',
            },
            procedure: 'local_1',
            scope: 'global',
            version: '1',
        }, 'test');
        expect(data).toMatchObject({
            authorization: '123',
        });
    }));
    it('onHandlerError option handles errors', () => __awaiter(void 0, void 0, void 0, function* () {
        const server = new RPCServer({
            displayName: 'test',
            handlers: {
                onHandlerError: (error, request) => {
                    return {
                        code: 599, //arbitrary code for test
                        data: request.procedure,
                        success: false,
                        message: error.message,
                    };
                },
            },
            ephemeralId: Math.random().toString(),
        });
        server.registerHandler({ procedure: 'error', scope: 'global', version: '1' }, (req) => __awaiter(void 0, void 0, void 0, function* () {
            if (req.args) {
                return {
                    code: 200,
                    success: true,
                };
            }
            throw new Error('some-error');
        }));
        try {
            yield server.call({
                args: false,
                procedure: 'error',
                scope: 'global',
                version: '1',
            }, 'test');
        }
        catch (e) {
            expect(e.code).toEqual(599);
            expect(e.data).toEqual('error');
            expect(e.message).toEqual('some-error');
            expect(e.success).toBeFalsy();
        }
        const { code, success } = yield server.call({
            args: true,
            procedure: 'error',
            scope: 'global',
            version: '1',
        }, 'test');
        expect(code).toEqual(200);
        expect(success).toBeTruthy();
    }));
    // this was removed and needs reimplementation for performance concerns using EventEmitter API
    // it('allows event listener', (done) => {
    //   const server = new RPCServer({
    //     displayName: 'test',
    //     ephemeralId: Math.random().toString(),
    //   })
    //
    //   const requestInfo = { procedure: 'test', scope: 'test', version: '1' }
    //
    //   server.registerHandler(requestInfo, async (request) => {
    //     throw new Error('some error')
    //   })
    //
    //   const unsubscribed = server.on(
    //     RPCServer.events.handler_error,
    //     (payload) => {
    //       expect(payload.error).toBeInstanceOf(Error)
    //       done()
    //     },
    //   )
    //
    //   server.call(requestInfo, 'test').catch()
    // })
});
//# sourceMappingURL=RPCServer.test.js.map