export const RPCEventTopics = {
    handler_call_success: 'handler_call_success',
    handler_error: 'handler_error',
    rpc_server_error: 'rpc_server_error',
};
export function makeHandlerCallSuccessPayload(payload) {
    return payload;
}
export function makeHandlerErrorPayload(payload) {
    return payload;
}
export function makeRPCServerErrorPayload(payload) {
    return payload;
}
//# sourceMappingURL=Events.js.map