'use strict';

var ObjectId = require('bson-objectid');
var Ajv = require('ajv');
var Debug = require('debug');
var amqp = require('amqp-connection-manager');
var Http = require('http');
var Koa = require('koa');
var cors = require('@koa/cors');
var koaBodyParser = require('koa-bodyparser');
var logger = require('koa-logger');
var WebSocket = require('ws');
var uuid = require('node-uuid');
var lodash = require('lodash');
var os = require('os');
var mongoose = require('mongoose');

const DEFAULT_REQUEST_SCOPE = 'global';
const DEFAULT_REQUEST_VERSION = '1';
class CallRequest {
    /**
     * Check if an object is a valid call request
     * @param obj
     */
    static isCallRequest(obj) {
        if (typeof obj === 'object') {
            return (obj.hasOwnProperty('procedure') &&
                obj.hasOwnProperty('scope') &&
                obj.hasOwnProperty('version') &&
                obj.hasOwnProperty('trace'));
        }
        return false;
    }
    constructor(request) {
        this.args = request.args;
        this.correlationId = request.correlationId;
        this.identity = request.identity;
        this.procedure = request.procedure;
        this.scope = request.scope || DEFAULT_REQUEST_SCOPE;
        this.version = request.version || DEFAULT_REQUEST_VERSION;
        this.trace = request.trace;
    }
}
CallRequest.fromCallRequestDTO = (callRequestDTO, details) => {
    var _a;
    if (!callRequestDTO.procedure) {
        throw new Error('procedure is required');
    }
    else if (!((_a = details === null || details === void 0 ? void 0 : details.trace) === null || _a === void 0 ? void 0 : _a.caller)) {
        throw new Error('trace.caller is required');
    }
    return new CallRequest({
        args: callRequestDTO.args,
        correlationId: callRequestDTO.correlationId,
        identity: callRequestDTO.identity,
        procedure: callRequestDTO.procedure,
        scope: callRequestDTO.scope,
        version: callRequestDTO.version,
        trace: {
            caller: details.trace.caller,
            id: callRequestDTO.correlationId || new ObjectId().toString(),
            internal: !!details.trace.internal,
            ipAddress: details.trace.ipAddress,
        },
    });
};
CallRequest.EMPTY = new CallRequest({
    procedure: 'empty',
    trace: {
        caller: 'empty',
        id: '0',
    },
});
CallRequest.toCallRequestDescription = (request) => {
    return {
        procedure: request.procedure,
        scope: request.scope || DEFAULT_REQUEST_SCOPE,
        version: request.version || DEFAULT_REQUEST_VERSION,
    };
};

class CallResponse {
    /**
     * Check if an object is a valid call response
     * @param obj
     */
    static isCallResponse(obj) {
        if (obj instanceof CallResponse || typeof obj === 'object') {
            return (obj.hasOwnProperty('code') &&
                obj.hasOwnProperty('success') &&
                obj.hasOwnProperty('trace'));
        }
        return false;
    }
    /**
     * Check if an object is a valid call response DTO
     * @param obj
     */
    static isCallResponseDTO(obj) {
        if (typeof obj === 'object') {
            return obj.hasOwnProperty('code') && obj.hasOwnProperty('success');
        }
        return false;
    }
    constructor(response, request) {
        this.code = response.code;
        this.correlationId = request.correlationId || response.correlationId;
        this.data = response.data;
        this.message = response.message;
        this.success = response.success;
        this.trace = {
            id: request.trace.id,
        };
    }
}
CallResponse.EMPTY = new CallResponse({
    code: 500,
    message: 'empty',
    success: false,
}, CallRequest.EMPTY);
CallResponse.toCallResponseDTO = (response) => {
    return {
        code: response.code,
        correlationId: response.correlationId,
        data: response.data,
        message: response.message,
        success: response.success,
    };
};

/******************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise, SuppressedError, Symbol */


function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

typeof SuppressedError === "function" ? SuppressedError : function (error, suppressed, message) {
    var e = new Error(message);
    return e.name = "SuppressedError", e.error = error, e.suppressed = suppressed, e;
};

function makeHandlerRequestKey(request) {
    return `${request.scope}::${request.procedure}::${request.version}`;
}

const ajv = new Ajv({
    coerceTypes: 'array',
    removeAdditional: true,
    useDefaults: true, // ie: convert { name: null } to { name: "" } if the the name=string type
});
const debug$6 = Debug('rpc:LocalHandlerManager');
class LocalHandlerManager {
    constructor(options) {
        this.options = options;
        this.handlersByHandlerRequestKey = {};
        this.handlerRequestValidationsByHandlerRequestKey = {};
        this.checkCanHandleRequest = (request) => {
            return !!this.handlersByHandlerRequestKey[makeHandlerRequestKey(request)];
        };
        this.getRegisteredHandlers = () => {
            return Object.keys(this.handlersByHandlerRequestKey);
        };
        this.getSimilarRegisteredProcedures = (request) => {
            const rgx = new RegExp(request.procedure);
            return Object.keys(this.handlersByHandlerRequestKey)
                .filter((key) => {
                return rgx.test(key);
            })
                .join(', ');
        };
        this.manageRequest = (request) => __awaiter(this, void 0, void 0, function* () {
            debug$6('manageRequest', request);
            try {
                const key = makeHandlerRequestKey(CallRequest.toCallRequestDescription(request));
                if (this.handlersByHandlerRequestKey[key].internalOnly &&
                    !request.trace.internal) {
                    debug$6('handler is marked "internalOnly" and the request is not coming from internal', request);
                    throw new CallResponse({
                        code: 403,
                        message: 'caller is not authorized to access this RPC',
                        success: false,
                    }, request);
                }
                if (this.handlerRequestValidationsByHandlerRequestKey[key]) {
                    this.handlerRequestValidationsByHandlerRequestKey[key](request);
                }
                return yield this.handlersByHandlerRequestKey[key].handler(request);
            }
            catch (e) {
                if (CallResponse.isCallResponse(e)) {
                    return e;
                }
                else if (CallResponse.isCallResponseDTO(e)) {
                    return new CallResponse(e, request);
                }
                console.log('LocalHandlerManager error: catching and returning 500', e);
                return new CallResponse({
                    code: 500,
                    success: false,
                }, request);
            }
        });
        this.registerHandler = (request, handler) => {
            var _a;
            debug$6('registerHandler', request);
            const key = makeHandlerRequestKey(request);
            this.handlersByHandlerRequestKey[key] = {
                internalOnly: !!request.internal,
                handler,
            };
            // Check if the HandlerRegistrationInfo has "args" as JSON-schema for automatic validation
            if (typeof request.args === 'object' && ((_a = request.args) === null || _a === void 0 ? void 0 : _a.type)) {
                try {
                    const validate = ajv.compile(request.args);
                    this.handlerRequestValidationsByHandlerRequestKey[key] = (request) => {
                        var _a;
                        const valid = validate(request.args || {});
                        if (!valid) {
                            debug$6('validation error', validate.errors);
                            throw new CallResponse({
                                code: 422,
                                message: `"args" validation failed: ${(_a = validate.errors) === null || _a === void 0 ? void 0 : _a.map((errorObj) => errorObj.instancePath + errorObj.message).join(', ')}`,
                                success: false,
                            }, request);
                        }
                        return undefined;
                    };
                }
                catch (e) {
                    console.log(``);
                    console.log(`ERROR: Unable to compile JSON-schema for "${key}"`);
                    console.log(`   ${e.message}`);
                    console.log(``);
                    // do not throw, exist the process, tests expect this impl
                    process.exit(1);
                }
            }
        };
    }
}

const DEFAULT_TIMEOUT = 30000;
class PromiseTimeoutError extends Error {
}
class PromiseWrapper {
    constructor(timeout) {
        this.reject = (rejectReturnValue) => {
            if (this.rejectPromise) {
                this.rejectPromise(rejectReturnValue);
            }
        };
        this.resolve = (resolveReturnValue) => {
            if (this.resolvePromise) {
                this.resolvePromise(resolveReturnValue);
            }
        };
        this.promise = new Promise((resolve, reject) => {
            const timer = setTimeout(() => {
                reject(new PromiseTimeoutError('PromiseWraper timeout'));
            }, timeout || DEFAULT_TIMEOUT);
            this.rejectPromise = (arg) => {
                clearTimeout(timer);
                reject(arg);
            };
            this.resolvePromise = (arg) => {
                clearTimeout(timer);
                resolve(arg);
            };
        });
    }
}

const RPC_GLOBAL_EXCHANGE_NAME = 'rpc-global'; // for requests that have no "scope"
const RPC_SCOPED_EXCHANGE_NAME = 'rpc-scoped'; // for requests that have a "scope"
const debug$5 = Debug('rpc:RabbitRpc');
var QueueMessageTypes;
(function (QueueMessageTypes) {
    QueueMessageTypes["Request"] = "request";
    QueueMessageTypes["Response"] = "response";
})(QueueMessageTypes || (QueueMessageTypes = {}));
class RabbitRpc {
    constructor(options) {
        this.options = options;
        this.pendingPromisesForResponse = {};
        this.sendRequestForResponse = (request) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            debug$5('sendRequestForResponse()');
            // await this.channelWrapper.waitForConnect()
            const promiseWrapper = new PromiseWrapper();
            this.pendingPromisesForResponse[request.trace.id] = promiseWrapper;
            const exchange = request.scope === DEFAULT_REQUEST_SCOPE
                ? RPC_GLOBAL_EXCHANGE_NAME
                : RPC_SCOPED_EXCHANGE_NAME;
            yield this.channelWrapper.publish(exchange, this.makeExchangeRoutingKey(CallRequest.toCallRequestDescription(request)), this.makeQueueRPCObject({ request }), {
                appId: this.options.config.displayName,
                correlationId: (_a = request === null || request === void 0 ? void 0 : request.trace) === null || _a === void 0 ? void 0 : _a.id,
                mandatory: true, // rabbit will return if cannot be routed, this.handleReturnedQueueMsg
                replyTo: this.uniqueQueueName,
                type: QueueMessageTypes.Request,
            });
            try {
                return yield promiseWrapper.promise;
            }
            catch (e) {
                let message = '';
                if (e instanceof PromiseTimeoutError) {
                    message = 'no response received (timed out)';
                }
                else {
                    message = 'there was an error processing RPC';
                }
                return new CallResponse({
                    code: 500,
                    message,
                    success: false,
                }, request);
            }
        });
        this.shutdown = () => this.channelWrapper.close();
        this.subscribeToHandleRequest = (request) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            debug$5('subscribeToHandleRequest', request);
            if (!this.channelWrapper) {
                throw new Error('cannot subscribe request handler because rabbitmq was not initialized properly');
            }
            (_a = this.channelWrapper) === null || _a === void 0 ? void 0 : _a.addSetup((channel) => __awaiter(this, void 0, void 0, function* () {
                const exchange = request.scope === DEFAULT_REQUEST_SCOPE
                    ? RPC_GLOBAL_EXCHANGE_NAME
                    : RPC_SCOPED_EXCHANGE_NAME;
                yield channel.bindQueue(this.uniqueQueueName, exchange, this.makeExchangeRoutingKey(request));
            }));
        });
        this.error = (...args) => console.log('RabbitRpc ERROR', ...args);
        this.handleReturnedQueueMsg = (msg) => __awaiter(this, void 0, void 0, function* () {
            debug$5('handleReturnedQueueMsg()');
            const { request } = this.parseQueueMsgContent(msg);
            if (!request) {
                this.error('rcvd returned queue msg and cannot handle', msg);
                return;
            }
            if (this.pendingPromisesForResponse[request.trace.id]) {
                this.pendingPromisesForResponse[request.trace.id].resolve(new CallResponse({
                    code: 501,
                    message: 'no registered handlers for RPC',
                    success: false,
                }, request));
            }
            else {
                debug$5('a queue message was returned but no pending requests found', request, msg);
            }
        });
        this.handleRequestFromQueue = (msg) => __awaiter(this, void 0, void 0, function* () {
            debug$5('handleRequestFromQueue()');
            const { properties } = msg;
            const { request } = this.parseQueueMsgContent(msg);
            if (!request) {
                this.error('rcvd queue msg that is not a valid RPC request', msg);
                return;
            }
            let response;
            try {
                response = yield this.requestHandler(request);
                if (!response) {
                    throw new Error('request handler provided no response');
                }
            }
            catch (e) {
                this.error('error handling request from the queue', e);
                this.error('   request: ', request);
            }
            try {
                if (response) {
                    yield this.sendHandledResponseToQueue(properties.replyTo, response, request);
                }
            }
            catch (e) {
                this.error('error sending response to the queue', e);
                this.error('   response: ', response);
            }
        });
        this.handleResponseFromQueue = (msg) => __awaiter(this, void 0, void 0, function* () {
            debug$5('handleResponseFromQueue()');
            const { response, request } = this.parseQueueMsgContent(msg);
            if (!request || !response) {
                this.error('rcvd queue msg that is not a valid RPC response', msg);
                return;
            }
            if (this.pendingPromisesForResponse[response.trace.id]) {
                this.pendingPromisesForResponse[response.trace.id].resolve(response);
            }
            else {
                this.error('received queue msg response with no pending request found locally', request, response);
            }
        });
        this.makeExchangeRoutingKey = (request) => makeHandlerRequestKey(request);
        /**
         * This method creates the structure for objects that we send over the queue
         * @param request {CallRequest}
         * @param response {CallResponse}
         */
        this.makeQueueRPCObject = ({ request, response, }) => ({
            request,
            response,
        });
        this.onReturnedQueueMsg = (msg) => {
            debug$5('onReturnedQueueMsg()');
            this.handleReturnedQueueMsg(msg);
        };
        this.onMsgReceivedFromQueue = (msg) => __awaiter(this, void 0, void 0, function* () {
            debug$5('handleMsgFromQueue() message', msg);
            if (!msg) {
                debug$5('queue consumer received empty msg');
                return;
            }
            switch (msg.properties.type) {
                case QueueMessageTypes.Response:
                    this.handleResponseFromQueue(msg);
                    break;
                case QueueMessageTypes.Request:
                    this.handleRequestFromQueue(msg);
                    break;
                default:
                    this.error('rcvd rabbit queue msg with unrecognized "type" property: ', msg);
            }
        });
        this.parseQueueMsgContent = (msg) => {
            try {
                let json;
                json = JSON.parse(msg.content.toString());
                if (!json.request) {
                    debug$5('parseQueueMsg() json has no "request" prop', json);
                    return {};
                }
                const response = json.response
                    ? new CallResponse(json.response, json.request)
                    : undefined;
                const request = new CallRequest(json.request);
                return {
                    response,
                    request,
                };
            }
            catch (e) {
                this.error('unable to parse queue msg content', e, msg);
                return {};
            }
        };
        this.sendHandledResponseToQueue = (queueName, response, request) => __awaiter(this, void 0, void 0, function* () {
            var _a;
            debug$5('sendHandledResponseToQueue() sending response for a request', request);
            yield this.channelWrapper.sendToQueue(queueName, this.makeQueueRPCObject({ request, response }), {
                appId: this.options.config.displayName,
                correlationId: (_a = request === null || request === void 0 ? void 0 : request.trace) === null || _a === void 0 ? void 0 : _a.id,
                type: QueueMessageTypes.Response,
            });
        });
        this.uniqueQueueName = `${options.config.displayName}-rpc-${new ObjectId().toString()}`;
        this.requestHandler = options.requestHandler;
        this.channelWrapper = options.connection.createChannel({
            json: true,
            setup: (channel) => {
                // @ts-ignore
                channel.addListener('return', this.onReturnedQueueMsg);
                return Promise.all([
                    channel.assertExchange(RPC_GLOBAL_EXCHANGE_NAME, 'direct'),
                    channel.assertExchange(RPC_SCOPED_EXCHANGE_NAME, 'direct'),
                    channel.assertQueue(this.uniqueQueueName, {
                        autoDelete: true,
                        durable: false,
                        // deadLetterExchange?: string; // todo
                        // deadLetterRoutingKey?: string;
                        // maxPriority?: number; // todo
                    }),
                    channel.consume(this.uniqueQueueName, this.onMsgReceivedFromQueue, {
                        noAck: true,
                    }),
                ]);
            },
        });
    }
}

const debug$4 = Debug('rpc:RabbitManager');
const error = (...args) => console.log('RabbitManager Err:', ...args);
class RabbitManager {
    constructor(options) {
        var _a, _b;
        this.options = options;
        this.connected = false;
        this.sendRequestForResponse = (request) => {
            return this.rpc.sendRequestForResponse(request);
        };
        this.shutdown = () => __awaiter(this, void 0, void 0, function* () {
            // cannot restart once this has been called
            try {
                yield this.rpc.shutdown();
                yield this.connection.close();
            }
            finally {
            }
        });
        this.subscribeToHandleRequest = (request) => {
            return this.rpc.subscribeToHandleRequest(request);
        };
        options.requestHandler;
        this.uniqueQueueName = `${options.config.displayName}-${options.config.ephemeralId}`;
        if (!((_b = (_a = options.config) === null || _a === void 0 ? void 0 : _a.messageBroker) === null || _b === void 0 ? void 0 : _b.amqpURI)) {
            throw new Error('amqpURI is required');
        }
        this.connection = amqp.connect([options.config.messageBroker.amqpURI], {
            // credentials: {
            //   username: '',
            //   password: ''
            // },
            heartbeatIntervalInSeconds: 5,
        });
        this.connection.on('connect', ({ url }) => {
            this.connected = true;
            debug$4('rabbit connected to: ', url);
        });
        this.connection.on('disconnect', ({ err }) => {
            if (this.connected) {
                error('disconnected from rabbit broker');
            }
            else {
                error('failed to connect to rabbit broker');
            }
        });
        this.rpc = new RabbitRpc({
            config: options.config,
            connection: this.connection,
            requestHandler: options.requestHandler,
        });
    }
}

/**
 * The MessageBrokerManager is responsible for managing the messageBroker in the
 * server (rabbitmq, kafka, redis, etc.).
 */
class MessageBrokerManager {
    constructor(options) {
        var _a;
        this.options = options;
        this.manageRequest = (request) => __awaiter(this, void 0, void 0, function* () {
            return this.messageQueue.sendRequestForResponse(request);
        });
        this.registerHandler = (request) => __awaiter(this, void 0, void 0, function* () {
            return this.messageQueue.subscribeToHandleRequest(request);
        });
        if (!((_a = options.config.messageBroker) === null || _a === void 0 ? void 0 : _a.amqpURI))
            throw new Error('cannot be initialized without config.messageBroker.amqpURI connection info for a MessageBroker');
        this.messageQueue = new RabbitManager(options);
    }
}

const debug$3 = Debug('rpc:CallManager');
class CallManager {
    constructor(options) {
        var _a;
        this.options = options;
        this.getRegisteredHandlers = () => this.localHandlerManager.getRegisteredHandlers();
        this.manageRequest = (request) => __awaiter(this, void 0, void 0, function* () {
            var _a, _b, _c, _d;
            const localHandler = this.localHandlerManager.checkCanHandleRequest(CallRequest.toCallRequestDescription(request));
            debug$3('manageRequest', request);
            const startRequestValue = (_b = (_a = this.options.config.handlers) === null || _a === void 0 ? void 0 : _a.onHandlerStartRequest) === null || _b === void 0 ? void 0 : _b.call(_a, request);
            if (localHandler) {
                debug$3('manageRequest, local handler found');
                return this.localHandlerManager.manageRequest(request).finally(() => {
                    var _a, _b;
                    (_b = (_a = this.options.config.handlers) === null || _a === void 0 ? void 0 : _a.onHandlerEndRequest) === null || _b === void 0 ? void 0 : _b.call(_a, request, startRequestValue);
                });
            }
            if (this.messageBrokerManager) {
                debug$3('manageRequest, sending to MessageBrokerManager');
                return this.messageBrokerManager.manageRequest(request).finally(() => {
                    var _a, _b;
                    (_b = (_a = this.options.config.handlers) === null || _a === void 0 ? void 0 : _a.onHandlerEndRequest) === null || _b === void 0 ? void 0 : _b.call(_a, request, startRequestValue);
                });
            }
            else {
                debug$3(`no messageBrokerManager found to proxy request, returning 501 no handler found (not implemented) for ${makeHandlerRequestKey(request)}`);
                (_d = (_c = this.options.config.handlers) === null || _c === void 0 ? void 0 : _c.onHandlerEndRequest) === null || _d === void 0 ? void 0 : _d.call(_c, request, startRequestValue);
                return new CallResponse({
                    code: 501,
                    message: `No procedure handler found for scope=${request.scope}, procedure=${request.procedure}, version=${request.version}. Trace info: caller=${request.trace.caller} internal=${!!request
                        .trace.internal}`,
                    success: false,
                }, request);
            }
        });
        this.registerHandler = (request, handler) => {
            this.localHandlerManager.registerHandler(request, handler);
            if (this.messageBrokerManager) {
                this.messageBrokerManager.registerHandler(request);
            }
        };
        this.localHandlerManager = new LocalHandlerManager({
            config: options.config,
        });
        if ((_a = options.config) === null || _a === void 0 ? void 0 : _a.messageBroker) {
            this.messageBrokerManager = new MessageBrokerManager({
                config: options.config,
                requestHandler: this.manageRequest,
            });
        }
    }
}

const DEFAULT_BIND = '127.0.0.1';

var _a;
const DEFAULT_HOST = '127.0.0.1';
class TelemetryHttpServer {
    constructor(options) {
        this.options = options;
        this.start = (onStartedCallback) => {
            var _b;
            if (this.koaAppListener)
                return;
            _a.configureKoaAppRoutes(this.koaApp, this.telemetryDataManager);
            if ('host' in this.options.config.server) {
                const host = ((_b = this.options.config.server) === null || _b === void 0 ? void 0 : _b.host) || DEFAULT_HOST;
                const port = this.options.config.server.port;
                this.koaAppListener = this.koaApp.listen({
                    host,
                    port,
                }, () => {
                    this.log(`Telemetry HTTP server listening ${host}:${port}`);
                    if (onStartedCallback) {
                        onStartedCallback();
                    }
                });
            }
        };
        this.stop = (onStoppedCallback) => {
            var _b;
            (_b = this.koaAppListener) === null || _b === void 0 ? void 0 : _b.close((err) => {
                if (err) {
                    console.error(`telemetry server had an error when stopping`, err);
                }
                else {
                    this.log(`telemetry server stopped`);
                    this.koaAppListener = undefined;
                    if (onStoppedCallback) {
                        onStoppedCallback();
                    }
                }
            });
        };
        this.log = (...args) => {
            console.log(`TelemetryServer `, ...args);
        };
        this.koaApp = new Koa();
        this.koaApp.use(cors());
        this.koaApp.use(logger());
        this.telemetryDataManager = options.telemetryDataManager;
    }
}
_a = TelemetryHttpServer;
TelemetryHttpServer.configureKoaAppRoutes = (app, telemetryDataManager) => {
    app.use((ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
        if (ctx.method !== 'GET')
            return yield next();
        switch (ctx.request.path) {
            case '/':
                ctx.status = 307;
                ctx.redirect(`https://rpc-docs.dev/?server=${ctx.request.origin || ctx.request.originalUrl || ctx.request.protocol + '://' + ctx.request.host}`);
                return;
            case '/docs':
                try {
                    ctx.body = yield telemetryDataManager.getRPCDocs();
                    ctx.status = 200;
                }
                catch (e) {
                    console.log('TelemetryHttpServer error fetching procedure docs:', e);
                    ctx.status = 500;
                }
                return;
            case '/health':
                ctx.body = 'ok';
                ctx.status = 200;
                return;
            case '/heartbeats':
                try {
                    const docs = yield telemetryDataManager.getHeartbeats();
                    ctx.body = docs;
                    ctx.status = 200;
                }
                catch (e) {
                    console.log('TelemetryHttpServer error fetching heartbeats:', e);
                    ctx.status = 500;
                }
                return;
        }
        return yield next();
    }));
};

const debug$2 = Debug('rpc:TelemetryHttpServer');
class HttpServer {
    constructor(options) {
        var _a, _b;
        this.options = options;
        this.serverListening = false;
        this.name = 'HttpServer';
        this.setIncomingRequestHandler = (requestHandler) => {
            this.requestHandler = requestHandler;
        };
        this.start = (onStartedCallback) => {
            var _a, _b, _c, _d;
            if (!this.requestHandler) {
                throw new Error(`No request handler has be set`);
            }
            if (this.serverListening) {
                debug$2('cannot start again, already runnning');
                return;
            }
            const host = ((_b = (_a = this.options.config.server) === null || _a === void 0 ? void 0 : _a.http) === null || _b === void 0 ? void 0 : _b.host) || DEFAULT_BIND;
            const port = (_d = (_c = this.options.config.server) === null || _c === void 0 ? void 0 : _c.http) === null || _d === void 0 ? void 0 : _d.port;
            this.httpServer.listen({
                host,
                port,
            }, () => {
                debug$2(`${this.options.config.displayName} listening ${host}:${port}`);
                this.serverListening = true;
                if (onStartedCallback) {
                    onStartedCallback();
                }
            });
        };
        this.stop = (onStoppedCallback) => {
            var _a, _b, _c, _d, _e;
            const host = ((_b = (_a = this.options.config.server) === null || _a === void 0 ? void 0 : _a.http) === null || _b === void 0 ? void 0 : _b.host) || DEFAULT_BIND;
            const port = (_d = (_c = this.options.config.server) === null || _c === void 0 ? void 0 : _c.http) === null || _d === void 0 ? void 0 : _d.port;
            (_e = this.httpServer) === null || _e === void 0 ? void 0 : _e.close((err) => {
                if (err) {
                    debug$2(`error when stopping (${host}:${port})`, err);
                }
                else {
                    debug$2(`stopped (${host}:${port})`);
                    this.serverListening = false;
                }
                if (onStoppedCallback) {
                    onStoppedCallback();
                }
            });
        };
        this.setup = () => {
            this.koaApp.use((ctx) => __awaiter(this, void 0, void 0, function* () {
                debug$2('received request');
                if (ctx.request.path === '/health') {
                    debug$2('returning health check status');
                    ctx.body = 'ok';
                    ctx.status = 200;
                    return;
                }
                if (!ctx.request.body ||
                    typeof ctx.request.body !== 'object' ||
                    !Object.keys(ctx.request.body).length) {
                    debug$2('empty request body received', ctx.request.body);
                    ctx.body = {
                        code: 400,
                        message: 'Empty request body received',
                        success: false,
                    };
                    return;
                }
                const requestDTO = ctx.request.body;
                let request;
                try {
                    request = CallRequest.fromCallRequestDTO(requestDTO, {
                        trace: {
                            caller: 'HttpServer',
                            ipAddress: HttpServer.parseIPAddressFromHttpReq(ctx.req),
                        },
                    });
                    debug$2(`request "${makeHandlerRequestKey(request)}"`);
                }
                catch (e) {
                    debug$2('error converting request body to CallRequest', e);
                    ctx.body = {
                        code: 400,
                        correlationId: request === null || request === void 0 ? void 0 : request.correlationId,
                        message: 'unable to parse request body as a valid CallRequest',
                        success: false,
                    };
                    return;
                }
                try {
                    debug$2('sending request to requestHandler');
                    const response = yield this.requestHandler(request);
                    debug$2('returning request response');
                    ctx.body = CallResponse.toCallResponseDTO(response);
                }
                catch (e) {
                    console.log('HttpSerer error: there was an error handling request:', e, request);
                    ctx.body = {
                        code: 500,
                        correlationId: request === null || request === void 0 ? void 0 : request.correlationId,
                        message: 'There was a problem calling the procedure',
                        success: false,
                    };
                }
                return;
            }));
        };
        this.koaApp = new Koa({ proxy: true });
        this.koaApp.use(koaBodyParser());
        this.koaApp.use(cors({ maxAge: 600, origin: '*' }));
        this.koaApp.use((ctx, next) => __awaiter(this, void 0, void 0, function* () {
            try {
                yield next();
            }
            catch (err) {
                console.log('There was an error in the Koa app from an incoming request', err);
                ctx.body = {
                    code: 400,
                    message: 'unable to parse & handle request',
                    success: false,
                };
            }
        }));
        if (((_b = (_a = options.config.server) === null || _a === void 0 ? void 0 : _a.http) === null || _b === void 0 ? void 0 : _b.serveDocs) &&
            options.telemetryManager) {
            TelemetryHttpServer.configureKoaAppRoutes(this.koaApp, options.telemetryManager.telemetryDataManager);
        }
        this.httpServer = Http.createServer(this.koaApp.callback());
        this.setup();
    }
    static parseIPAddressFromHttpReq(req) {
        let ip = '';
        if (req.headers['x-real-ip']) {
            if (Array.isArray(req.headers['x-real-ip'])) {
                ip = req.headers['x-real-ip'][0];
            }
            else {
                ip = req.headers['x-real-ip'].split(',')[0];
            }
        }
        else {
            ip = req.socket.remoteAddress || '';
        }
        ip = ip.trim();
        return ip;
    }
}

function isIdentityValid(identity) {
    if (!identity)
        return false;
    if (identity.authorization && typeof identity.authorization !== 'string')
        return false;
    if (identity.deviceName && typeof identity.deviceName !== 'string')
        return false;
    if (identity.metadata && typeof identity.metadata !== 'object')
        return false;
    return true;
}

const debug$1 = Debug('rpc:WebSocketServer');
class WebSocketServer {
    constructor(options) {
        var _a, _b, _c, _d, _e, _f;
        this.options = options;
        this.clientMessageHandlers = [];
        this.connectionsByConnectionId = new Map();
        // a local flag to track if the server is shutting down gracefully
        this.wssShuttingDown = false;
        this.name = 'WebSocketServer';
        /**
         * This method allows adding a callback for websocket client messages to be handled that are not RPC requests.
         * @param handler
         */
        this.addClientMessageHandler = (handler) => {
            this.clientMessageHandlers.push(handler);
        };
        this.removeClientMessageHandler = (handler) => {
            const indexToRemove = this.clientMessageHandlers.findIndex((func) => func === handler);
            if (indexToRemove > -1) {
                this.clientMessageHandlers.splice(indexToRemove, 1);
            }
        };
        this.setIncomingRequestHandler = (requestHandler) => {
            this.requestHandler = requestHandler;
        };
        this.sendMessageToClient = (connectionId, data) => {
            var _a, _b, _c;
            const ws = this.connectionsByConnectionId.get(connectionId);
            if (ws) {
                if (ws.readyState !== WebSocket.OPEN) {
                    debug$1('WebSocketServer.sendMessageToClient() removing connectionId - ws.readyState is not OPEN:', connectionId, ws.readyState);
                    this.connectionsByConnectionId.delete(connectionId);
                    ws.terminate();
                    return;
                }
                let serverMessage;
                try {
                    serverMessage = JSON.stringify({ serverMessage: data });
                }
                catch (e) {
                    console.log('WebSocketServer.sendMessageToClient() error stringify serverMessage to send to client: ', serverMessage);
                }
                if (serverMessage) {
                    ws.send(serverMessage);
                }
            }
            else {
                console.warn('WebSocketServer.sendMessageToClient() removing connectionId - no ws found for connectionId:', connectionId);
                this.connectionsByConnectionId.delete(connectionId);
                if ((_c = (_b = (_a = this.options.config) === null || _a === void 0 ? void 0 : _a.server) === null || _b === void 0 ? void 0 : _b.websocket) === null || _c === void 0 ? void 0 : _c.onClientDisconnect) {
                    this.options.config.server.websocket.onClientDisconnect({
                        connectionId,
                    });
                }
            }
        };
        this.start = (onStartedCallback) => {
            this.wssShuttingDown = false;
            this.connectionsByConnectionId.clear();
            if (!this.requestHandler) {
                throw new Error(`No request handler has be set`);
            }
            this.wss = new WebSocket.Server({
                host: this.host,
                port: this.port,
            });
            this.wss.on('listening', () => {
                console.info(`RPC WebSocketServer listening ${this.host}:${this.port}`);
            });
            this.wss.on('error', (err) => {
                var _a;
                console.error(`RPC WebSocketServer error - stopping ws server...`, err);
                (_a = this.wss) === null || _a === void 0 ? void 0 : _a.close();
            });
            this.wss.on('close', () => {
                this.connectionsByConnectionId.clear();
                if (!this.wssShuttingDown) {
                    console.error(`RPC WebSocketServer closed unexpectedly - restarting ws server...`);
                    this.wss = undefined;
                    setTimeout(() => {
                        this.start();
                    }, 1000); // Reconnect after a delay
                }
            });
            this.wss.on('connection', (ws, req) => {
                var _a, _b, _c;
                const websocketId = uuid.v4();
                this.connectionsByConnectionId.set(websocketId, ws);
                let identity;
                const wsIp = WebSocketServer.parseIPAddressFromWsReq(req);
                debug$1('websocket connected, connectionId:', websocketId, ' ip:', wsIp);
                if ((_c = (_b = (_a = this.options.config) === null || _a === void 0 ? void 0 : _a.server) === null || _b === void 0 ? void 0 : _b.websocket) === null || _c === void 0 ? void 0 : _c.onClientConnect) {
                    this.options.config.server.websocket.onClientConnect({
                        connectionId: websocketId,
                        identity,
                        ip: wsIp,
                    });
                }
                ws.on('close', (code, data) => {
                    var _a, _b, _c;
                    this.connectionsByConnectionId.delete(websocketId);
                    const reason = code || data.toString();
                    debug$1('websocket disconnected, connectionId:', websocketId, reason);
                    if ((_c = (_b = (_a = this.options.config) === null || _a === void 0 ? void 0 : _a.server) === null || _b === void 0 ? void 0 : _b.websocket) === null || _c === void 0 ? void 0 : _c.onClientDisconnect) {
                        this.options.config.server.websocket.onClientDisconnect({
                            connectionId: websocketId,
                            identity,
                            ip: wsIp,
                        });
                    }
                });
                // when there is a client connection we need to handle it so it's not fatal
                ws.on('error', (err) => {
                    console.error(`RPC WebSocketServer connection error, ${wsIp}`, err);
                    this.connectionsByConnectionId.delete(websocketId);
                    ws.close();
                });
                ws.on('message', (data) => __awaiter(this, void 0, void 0, function* () {
                    var _a, _b, _c;
                    const message = data === null || data === void 0 ? void 0 : data.toString();
                    // debugMessages(
                    //   'received message from WS, connectionId:',
                    //   websocketId,
                    //   message,
                    // )
                    let messageParsed;
                    if (typeof message === 'string') {
                        try {
                            messageParsed = JSON.parse(message);
                            if (typeof messageParsed !== 'object' ||
                                Array.isArray(messageParsed)) {
                                // debugMessages(
                                //   'WebSocketServer error: received unparseable WS msg object',
                                //   message,
                                // )
                                return;
                            }
                        }
                        catch (e) {
                            // debugMessages(
                            //   'WebSocketServer error: received unparseable WS msg string',
                            //   message,
                            // )
                            return;
                        }
                    }
                    else {
                        // debugMessages(
                        //   'WebSocketServer error: received unknown "message" from client',
                        //   message,
                        // )
                        return;
                    }
                    const incomingMessage = messageParsed;
                    if (incomingMessage.identity) {
                        if (!isIdentityValid(incomingMessage.identity)) {
                            console.error('error receiving CallResponseDTO with invalid identity schema', incomingMessage.identity);
                            ws.send(JSON.stringify(CallResponse.toCallResponseDTO(new CallResponse({
                                code: 400,
                                message: 'invalid identity schema - disconnecting',
                                success: false,
                            }, CallRequest.fromCallRequestDTO(Object.assign(Object.assign({}, incomingMessage), { 
                                // we add a procedure because CallRequest.fromCallRequestDTO requires a procedure on incoming RPC's
                                procedure: '__internal-identity__' }), { trace: { caller: 'WebSocketServer' } })))), () => ws.close());
                            return;
                        }
                        // else {
                        //   debugMessages('received identity:', incomingMessage.identity)
                        // }
                        if ((identity === null || identity === void 0 ? void 0 : identity.authorization) &&
                            identity.authorization !== incomingMessage.identity.authorization) {
                            console.error('error, a websocket changed identity.authorization from previous value, disconnecting...');
                            ws.send(JSON.stringify(CallResponse.toCallResponseDTO(new CallResponse({
                                code: 400,
                                message: 'identity.authorization has changed - disconnecting websocket connection',
                                success: false,
                            }, CallRequest.fromCallRequestDTO(Object.assign(Object.assign({}, incomingMessage), { 
                                // we add a procedure because CallRequest.fromCallRequestDTO requires a procedure on incoming RPC's
                                procedure: '__internal-identity__' }), { trace: { caller: 'WebSocketServer' } })))), () => ws.close());
                            return;
                        }
                        if (incomingMessage.identity) {
                            if ((_c = (_b = (_a = this.options.config) === null || _a === void 0 ? void 0 : _a.server) === null || _b === void 0 ? void 0 : _b.websocket) === null || _c === void 0 ? void 0 : _c.onClientConnectionIdentityChanged) {
                                if (!lodash.isEqual(incomingMessage.identity, identity)) {
                                    this.options.config.server.websocket.onClientConnectionIdentityChanged({
                                        connectionId: websocketId,
                                        identity: incomingMessage.identity,
                                        ip: wsIp,
                                    });
                                }
                            }
                        }
                        identity = incomingMessage.identity;
                    }
                    if ('clientMessage' in incomingMessage) {
                        this.clientMessageHandlers.forEach((msgHandler) => {
                            try {
                                msgHandler(Object.assign(Object.assign({}, incomingMessage), { connectionId: websocketId, identity }));
                            }
                            catch (err) {
                                console.log('error calling clientMessageHandler() with incoming websocket client msg', err);
                            }
                        });
                        return;
                    }
                    const requestDTO = incomingMessage;
                    // This is just a "set my identity" msg from the client
                    if (!requestDTO.procedure && requestDTO.identity) {
                        try {
                            const response = new CallResponse({
                                code: 200,
                                correlationId: requestDTO.correlationId,
                                success: true,
                            }, CallRequest.fromCallRequestDTO(Object.assign(Object.assign({}, requestDTO), { 
                                // we add a procedure because CallRequest.fromCallRequestDTO requires a procedure on incoming RPC's
                                procedure: '__internal-set-identity__' }), { trace: { caller: 'WebSocketServer' } }));
                            ws.send(JSON.stringify(CallResponse.toCallResponseDTO(response)));
                        }
                        catch (e) {
                            console.log('error sending CallResponseDTO to WS connection after setting RPCClientIdentity, error:', e);
                        }
                        return;
                    }
                    let request;
                    try {
                        request = CallRequest.fromCallRequestDTO(Object.assign({ identity }, requestDTO), { trace: { caller: 'WebSocketServer', ipAddress: wsIp } });
                    }
                    catch (e) {
                        // debugMessages(
                        //   'WebSocketServer error: WS msg not a valid CallRequestDTO',
                        //   request,
                        //   e,
                        // )
                        return;
                    }
                    if (request) {
                        // debugMessages('handling received request')
                        if (request.identity) {
                            if (!isIdentityValid(request.identity)) {
                                console.log('error recieving CallResponseDTO with invalid RPCClientIdentity schema', request.identity);
                                return;
                            }
                            identity = request.identity;
                        }
                        const response = yield this.handleIncomingRequest(request);
                        // debugMessages('returning request response')
                        try {
                            ws.send(JSON.stringify(CallResponse.toCallResponseDTO(response)));
                        }
                        catch (e) {
                            console.error('WebSocketServer error: error sending CallResponseDTO to WS connection', response, e);
                        }
                        return;
                    }
                }));
            });
            this.wss.once('listening', () => {
                debug$1(`${this.options.config.displayName} listening ${this.host}:${this.port}`);
                if (onStartedCallback) {
                    onStartedCallback();
                }
            });
        };
        this.stop = (onStoppedCallback) => {
            var _a, _b, _c;
            this.wssShuttingDown = true;
            (_a = this.wss) === null || _a === void 0 ? void 0 : _a.close((err) => {
                if (err) {
                    console.log('WSS error onclose:', err);
                }
                if (onStoppedCallback)
                    onStoppedCallback();
            });
            for (const ws of (_c = (_b = this.wss) === null || _b === void 0 ? void 0 : _b.clients) !== null && _c !== void 0 ? _c : []) {
                ws.terminate();
            }
        };
        this.handleIncomingRequest = (request) => __awaiter(this, void 0, void 0, function* () {
            try {
                debug$1('sending request to requestHandler');
                return yield this.requestHandler(request);
            }
            catch (e) {
                if (e instanceof CallResponse) {
                    debug$1('handleIncomingRequest catching a throw CallResponse');
                    return e;
                }
                console.log('WebSocketServer error: there was an error handling request:', request, e);
                return new CallResponse({
                    code: 500,
                    message: 'There was a problem calling the procedure',
                    success: false,
                }, request);
            }
        });
        if (!((_b = (_a = options.config.server) === null || _a === void 0 ? void 0 : _a.websocket) === null || _b === void 0 ? void 0 : _b.port)) {
            throw new Error('config.gatewayServer.websocket.port is required');
        }
        this.host = ((_d = (_c = options.config.server) === null || _c === void 0 ? void 0 : _c.websocket) === null || _d === void 0 ? void 0 : _d.host) || DEFAULT_BIND;
        this.port = (_f = (_e = options.config.server) === null || _e === void 0 ? void 0 : _e.websocket) === null || _f === void 0 ? void 0 : _f.port;
        if (options.config.server.websocket.onClientMessage) {
            this.clientMessageHandlers.push(options.config.server.websocket.onClientMessage);
        }
    }
    static parseIPAddressFromWsReq(req) {
        let ip = '';
        if (req.headers['x-real-ip']) {
            if (Array.isArray(req.headers['x-real-ip'])) {
                ip = req.headers['x-real-ip'][0];
            }
            else {
                ip = req.headers['x-real-ip'].split(',')[0];
            }
        }
        else {
            ip = req.socket.remoteAddress || '';
        }
        ip = ip.trim();
        return ip;
    }
}

function deepFreeze(object) {
    const propNames = Object.getOwnPropertyNames(object);
    for (const name of propNames) {
        const value = object[name];
        if (value && typeof value === 'object') {
            deepFreeze(value);
        }
    }
    return Object.freeze(object);
}

const HeartbeatModel = mongoose.model('Heartbeat', new mongoose.Schema({
    // these documents will expire after 1min
    createdAt: { default: Date.now, type: Date, expires: 60 },
    hostName: String,
    ephemeralId: String,
    gatewayHttpServer: Boolean,
    gatewayWebSocketServer: Boolean,
    displayName: String,
    startTime: String,
}));
const RPCDocModel = mongoose.model('RPCDoc', new mongoose.Schema({
    args: mongoose.Schema.Types.Mixed,
    createdAt: String,
    data: mongoose.Schema.Types.Mixed,
    description: String,
    identity: [String],
    internal: Boolean,
    procedure: { required: true, type: String },
    scope: { default: 'global', type: String },
    serverDisplayName: String,
    version: { default: '1', type: String },
}));

class MongoManager {
    constructor(mongoURI) {
        this.mongoURI = mongoURI;
        this.showDbConnectionLogs = false;
        this.connected = false;
        this.setupMongoConnection = () => __awaiter(this, void 0, void 0, function* () {
            mongoose.connect(this.mongoURI, {
                autoCreate: true,
            });
            const db = mongoose.connection;
            db.on('disconnected', (info) => {
                if (this.showDbConnectionLogs) {
                    console.log('MongoManager disconnected:', info);
                }
                this.connected = false;
            });
            db.on('error', (err) => {
                console.error('MongoManager connection error:', err);
                this.connected = false;
            });
            db.once('connected', () => {
                if (this.showDbConnectionLogs) {
                    console.log('MongoManager connected:');
                }
                this.connected = true;
            });
        });
        this.tearDownConnection = () => {
            mongoose.disconnect();
        };
        this.showDbConnectionLogs = process.env.NODE_ENV !== 'test';
    }
}

class MongoTelemetryManager {
    constructor(adaptorConfig) {
        this.disconnect = () => {
            this.mongoManager.tearDownConnection();
        };
        this.getHeartbeats = () => __awaiter(this, void 0, void 0, function* () {
            return HeartbeatModel.find({}, null, { limit: 500 })
                .lean()
                .exec()
                .then((docs) => {
                return lodash.uniqBy(docs, 'ephemeralId');
            });
        });
        this.getProcedureDocs = () => __awaiter(this, void 0, void 0, function* () {
            return RPCDocModel.find({}, null, { limit: 5000 }).lean().exec();
        });
        this.saveHeartbeat = (telemetryHeartbeat) => __awaiter(this, void 0, void 0, function* () {
            const doc = new HeartbeatModel(telemetryHeartbeat);
            try {
                yield doc.save();
            }
            catch (err) {
                return console.error('MongoTelemetryManager#saveHeartbeat error:', err);
            }
        });
        this.saveHandlersRegistrationInfo = (procedureDescriptions) => __awaiter(this, void 0, void 0, function* () {
            if (!procedureDescriptions.length)
                return;
            let waitCount = 0;
            while (!this.mongoManager.connected) {
                if (waitCount > 30) {
                    throw Error('MongoManager has not connected to mongoDB');
                }
                waitCount++;
                yield new Promise((r) => setTimeout(r, 100));
            }
            try {
                const serverDisplayName = procedureDescriptions[0].serverDisplayName;
                yield RPCDocModel.deleteMany({ serverDisplayName });
                yield RPCDocModel.insertMany(procedureDescriptions);
            }
            catch (err) {
                console.error('MongoTelemetryManager#saveHandlerRegistrationInfo error:', err);
            }
        });
        this.mongoManager = new MongoManager(adaptorConfig.mongoURI);
        this.mongoManager.setupMongoConnection();
    }
}

class TelemetryDataManager {
    /**
     * If an adaptor is not provided, the TelemetryDataManager will use an in-memory store.
     * @param adaptor
     */
    constructor(adaptor) {
        this.inMemoryRPCDocs = [];
        this.disconnect = () => {
            var _a;
            (_a = this.mongoTelemetryManager) === null || _a === void 0 ? void 0 : _a.disconnect();
        };
        this.getHeartbeats = () => __awaiter(this, void 0, void 0, function* () {
            if (this.mongoTelemetryManager) {
                return this.mongoTelemetryManager.getHeartbeats();
            }
            return [];
        });
        this.getRPCDocs = () => __awaiter(this, void 0, void 0, function* () {
            if (this.mongoTelemetryManager) {
                return this.mongoTelemetryManager.getProcedureDocs();
            }
            return this.inMemoryRPCDocs;
        });
        this.saveHandlersRegistrationInfo = (procedureDescriptions) => __awaiter(this, void 0, void 0, function* () {
            if (!procedureDescriptions.length)
                return;
            if (this.mongoTelemetryManager) {
                yield this.mongoTelemetryManager.saveHandlersRegistrationInfo(procedureDescriptions);
            }
            else {
                this.inMemoryRPCDocs = this.inMemoryRPCDocs.concat(procedureDescriptions);
            }
        });
        this.saveHeartbeat = (heartbeat) => __awaiter(this, void 0, void 0, function* () {
            if (this.mongoTelemetryManager) {
                yield this.mongoTelemetryManager.saveHeartbeat(heartbeat);
            }
        });
        if (adaptor === null || adaptor === void 0 ? void 0 : adaptor.mongoURI) {
            this.mongoTelemetryManager = new MongoTelemetryManager({
                mongoURI: adaptor.mongoURI,
            });
        }
    }
}

class TelemetryManager {
    constructor(options) {
        var _a, _b, _c;
        this.options = options;
        this.heartbeatData = {
            hostName: os.hostname(),
            ephemeralId: '',
            gatewayHttpServer: false,
            gatewayWebSocketServer: false,
            displayName: '',
            startTime: '',
        };
        this.registeredHandlers = [];
        this.getHeartbeatData = () => this.heartbeatData;
        this.getRegisteredHandlers = () => this.registeredHandlers;
        this.registerHandler = (request) => {
            this.registeredHandlers.push(request);
        };
        this.startTelemetryReporting = () => __awaiter(this, void 0, void 0, function* () {
            this.heartbeatInterval = setInterval(() => {
                this.saveHeartbeat();
            }, 30000);
            yield this.saveHeartbeat();
            yield this.telemetryDataManager.saveHandlersRegistrationInfo(this.registeredHandlers.map((request) => (Object.assign(Object.assign({}, request), { createdAt: new Date().toISOString(), serverDisplayName: this.options.config.displayName }))));
        });
        this.saveHeartbeat = () => __awaiter(this, void 0, void 0, function* () {
            return this.telemetryDataManager.saveHeartbeat(this.heartbeatData);
        });
        this.telemetryDataManager = new TelemetryDataManager((_a = options.config.telemetry) === null || _a === void 0 ? void 0 : _a.adapter);
        this.heartbeatData.ephemeralId = options.config.ephemeralId || new Date().toISOString();
        this.heartbeatData.gatewayHttpServer = !!((_b = options.config.server) === null || _b === void 0 ? void 0 : _b.http);
        this.heartbeatData.gatewayWebSocketServer =
            !!((_c = options.config.server) === null || _c === void 0 ? void 0 : _c.websocket);
        this.heartbeatData.displayName = options.config.displayName;
        this.heartbeatData.startTime = new Date().toISOString();
    }
}

const debug = Debug('rpc:RPCServer');
class RPCServer {
    constructor(config) {
        this.serverStarted = false;
        this.call = (request, traceCaller) => __awaiter(this, void 0, void 0, function* () {
            if (!traceCaller) {
                throw Error('RPCServer.call() requires "traceCaller" param string to track where this call originated');
            }
            const response = yield this.callManager.manageRequest(CallRequest.fromCallRequestDTO(request, {
                trace: {
                    caller: `RPCServer.call ${traceCaller}`,
                    internal: true,
                },
            }));
            if (!response.success) {
                throw response;
            }
            return response;
        });
        this.getRegisteredHandlers = () => this.callManager.getRegisteredHandlers();
        /**
         * Register a handler for this RPCServer to handle and manage requests to/from.
         *
         * The handler callback provides an "call" method for registered handlers to make calls to other RPC handlers in
         * the same network. The incoming CallRequest's RPCClientIdentity will be used when making subsequent calls.
         *
         * @param request
         * @param handler
         */
        this.registerHandler = (request, handler) => {
            var _a;
            if (this.serverStarted) {
                throw Error('RPCServer: you cannot register a handler after the server has started.');
            }
            // wrap the handler so handler implementations can return a POJO
            const wrappedHandler = (request) => __awaiter(this, void 0, void 0, function* () {
                var _a;
                try {
                    let callResponse;
                    const originalRequest = request;
                    const reqKey = makeHandlerRequestKey(originalRequest);
                    debug(`wrappedHandler, Handler -> ${reqKey}`, {
                        args: request.args,
                        identity: request.identity,
                        trace: request.trace,
                    });
                    const internalCallWithCallerIdentity = (request) => {
                        return this.call(Object.assign({ identity: originalRequest.identity }, request), makeHandlerRequestKey(originalRequest));
                    };
                    const handlerResponse = yield handler(request, internalCallWithCallerIdentity);
                    if (handlerResponse instanceof CallResponse) {
                        callResponse = handlerResponse;
                    }
                    else if (typeof handlerResponse === 'object' &&
                        handlerResponse !== null &&
                        'code' in handlerResponse &&
                        'success' in handlerResponse) {
                        callResponse = new CallResponse(handlerResponse, request);
                    }
                    else {
                        console.log(new Error(`${reqKey} handler did not return a valid CallResponse or CallResponseDTO, "success" and "code" properties are required.`));
                        throw {
                            code: 500,
                            message: `RPC handler error`,
                            success: false,
                        };
                    }
                    if (!callResponse.success) {
                        throw callResponse;
                    }
                    return callResponse;
                }
                catch (e) {
                    debug(`wrappedHandler, caught error for request ${makeHandlerRequestKey(request)}`, e);
                    if (CallResponse.isCallResponse(e)) {
                        return e;
                    }
                    else if (CallResponse.isCallResponseDTO(e)) {
                        return new CallResponse(e, request);
                    }
                    else if ((_a = this.config.handlers) === null || _a === void 0 ? void 0 : _a.onHandlerError) {
                        try {
                            const onHandlerErrorResult = this.config.handlers.onHandlerError(e, request);
                            if (onHandlerErrorResult) {
                                if (onHandlerErrorResult instanceof CallResponse) {
                                    return onHandlerErrorResult;
                                }
                                if (CallResponse.isCallResponseDTO(onHandlerErrorResult)) {
                                    return new CallResponse(onHandlerErrorResult, request);
                                }
                            }
                        }
                        catch (e) {
                            console.log('LocalHandlerManager catch and call RPCService optional onHandlerError() error', e);
                        }
                    }
                    console.log('RPCServer wrappedHandler error: ', e, request);
                    return new CallResponse({ code: 500, message: 'server handler issue', success: false }, request);
                }
            });
            this.callManager.registerHandler(request, wrappedHandler);
            (_a = this.telemetryManager) === null || _a === void 0 ? void 0 : _a.registerHandler(request);
        };
        /**
         * Send a message to a specific RPC client by connectionId.
         * @param connectionId
         * @param data
         */
        this.sendMessageToClient = (connectionId, data) => {
            if (this.websocketServer) {
                this.websocketServer.sendMessageToClient(connectionId, data);
            }
        };
        this.start = () => {
            var _a, _b, _c;
            if (!this.serverStarted) {
                this.serverStarted = true;
                (_a = this === null || this === void 0 ? void 0 : this.httpServer) === null || _a === void 0 ? void 0 : _a.start();
                (_b = this === null || this === void 0 ? void 0 : this.websocketServer) === null || _b === void 0 ? void 0 : _b.start();
                (_c = this === null || this === void 0 ? void 0 : this.telemetryManager) === null || _c === void 0 ? void 0 : _c.startTelemetryReporting();
            }
            else {
                throw Error('RPCServer: cannot call start() more than once');
            }
        };
        this.stop = () => {
            var _a, _b;
            (_a = this === null || this === void 0 ? void 0 : this.httpServer) === null || _a === void 0 ? void 0 : _a.stop();
            (_b = this === null || this === void 0 ? void 0 : this.websocketServer) === null || _b === void 0 ? void 0 : _b.stop();
        };
        this.setupGatewayServer = () => {
            var _a, _b;
            const config = this.config;
            if (!(config === null || config === void 0 ? void 0 : config.messageBroker) && !config.server) {
                debug('no config.messageBroker or config.gatewayServer options, acting as standalone server');
            }
            if ((_a = config === null || config === void 0 ? void 0 : config.server) === null || _a === void 0 ? void 0 : _a.http) {
                this.httpServer = new HttpServer({
                    config: this.config,
                    telemetryManager: this.telemetryManager,
                });
                this.httpServer.setIncomingRequestHandler(this.callManager.manageRequest);
            }
            if ((_b = config === null || config === void 0 ? void 0 : config.server) === null || _b === void 0 ? void 0 : _b.websocket) {
                this.websocketServer = new WebSocketServer({
                    config: this.config,
                });
                this.websocketServer.setIncomingRequestHandler(this.callManager.manageRequest);
            }
        };
        if (!config.ephemeralId) {
            config.ephemeralId = new ObjectId().toString();
        }
        this.config = deepFreeze(config);
        if (!config.displayName) {
            throw new Error('config.displayName is required');
        }
        this.telemetryManager = new TelemetryManager({
            config: this.config,
        });
        this.callManager = new CallManager({
            config: this.config,
        });
        debug(`RPCServer id: ${this.config.ephemeralId}`);
        this.setupGatewayServer();
    }
}

class TelemetryServer {
    constructor(config) {
        this.start = () => __awaiter(this, void 0, void 0, function* () {
            var _a;
            (_a = this === null || this === void 0 ? void 0 : this.telemetryHttpServer) === null || _a === void 0 ? void 0 : _a.start();
        });
        this.stop = () => {
            var _a, _b;
            (_a = this.telemetryDataManager) === null || _a === void 0 ? void 0 : _a.disconnect();
            (_b = this === null || this === void 0 ? void 0 : this.telemetryHttpServer) === null || _b === void 0 ? void 0 : _b.stop();
        };
        this.config = deepFreeze(config);
        this.telemetryDataManager = new TelemetryDataManager(config.adapter);
        this.telemetryHttpServer = new TelemetryHttpServer({
            config: config,
            telemetryDataManager: this.telemetryDataManager,
        });
    }
}

exports.CallRequest = CallRequest;
exports.CallResponse = CallResponse;
exports.RPCServer = RPCServer;
exports.TelemetryServer = TelemetryServer;
//# sourceMappingURL=index.js.map
