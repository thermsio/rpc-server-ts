var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { RPCServer } from '../index';
import { CallResponse } from '../CallResponse';
const rpcHandlerServer = new RPCServer({
    displayName: 'example-handler',
    ephemeralId: Math.random().toString(),
    messageBroker: {
        // amqpURI: 'amqp://localhost:5672',
        amqpURI: 'amqp://76.178.162.56:5672',
    },
});
rpcHandlerServer.registerHandler({ procedure: 'login', scope: 'auth', version: '1' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
    return new CallResponse({
        code: 200,
        data: { id: (Math.random() * 100000).toString(), name: 'Tom C' },
        success: true,
    }, request);
}));
rpcHandlerServer.registerHandler({ procedure: 'get-users', scope: 'users', version: '1' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    if (!((_a = request.identity) === null || _a === void 0 ? void 0 : _a.authorization)) {
        return new CallResponse({
            code: 403,
            message: 'you are not authorized',
            success: false,
        }, request);
    }
    return new CallResponse({
        code: 200,
        data: [
            { id: (Math.random() * 100000).toString(), name: 'Tom C' },
            { id: (Math.random() * 100000).toString(), name: 'Zoo Randall' },
            { id: (Math.random() * 100000).toString(), name: 'Cory Robinson' },
        ],
        success: true,
    }, request);
}));
rpcHandlerServer.registerHandler({ procedure: 'get-user', scope: 'users', version: '1' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    if (!((_b = request.identity) === null || _b === void 0 ? void 0 : _b.authorization)) {
        return new CallResponse({
            code: 403,
            message: 'you are not authorized',
            success: false,
        }, request);
    }
    return new CallResponse({
        code: 200,
        data: { id: (Math.random() * 100000).toString(), name: 'Tom C' },
        success: true,
    }, request);
}));
rpcHandlerServer.start();
//# sourceMappingURL=example-handler-server.js.map