var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { RPCServer } from '../index';
import { CallResponse } from '../CallResponse';
const rpcGatewayServer = new RPCServer({
    displayName: 'example-gateway',
    ephemeralId: Math.random().toString(),
    server: {
        http: {
            port: 3000,
            serveLocalTelemetryClient: true,
        },
        websocket: {
            port: 3001,
        },
    },
    // messageBroker: {
    // amqpURI: 'amqp://localhost:5672',
    // },
});
rpcGatewayServer.registerHandler({ procedure: 'login', scope: 'auth', version: '1' }, (request) => __awaiter(void 0, void 0, void 0, function* () {
    return new CallResponse({
        code: 200,
        data: {
            id: (Math.random() * 100000).toString(),
            email: 'cory@therms.io',
            user: { name: 'Cory Robinson' },
        },
        success: true,
    }, request);
}));
rpcGatewayServer.start();
//# sourceMappingURL=example-gateway.js.map