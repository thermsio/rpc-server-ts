var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { TelemetryServer } from './TelemetryServer';
import axios from 'axios';
describe('TelemetryServer class', () => {
    let telemetryServer;
    let config = {
        adapter: {},
        debug: false,
        server: { host: 'localhost', port: 4300 },
    };
    beforeEach(() => {
        telemetryServer = new TelemetryServer(config);
    });
    test('correctly starts the telemetryHttpServer', () => __awaiter(void 0, void 0, void 0, function* () {
        yield telemetryServer.start();
        yield expect(axios.get('http://localhost:4300/docs')).resolves.not.toThrow();
        telemetryServer.stop();
    }));
});
//# sourceMappingURL=TelemetryServer.test.js.map