var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { deepFreeze } from '../utils/deep-freeze';
import { TelemetryDataManager } from './intrinsic/TelemetryDataManager';
import { TelemetryHttpServer } from './TelemetryHttpServer';
export class TelemetryServer {
    constructor(config) {
        this.start = () => __awaiter(this, void 0, void 0, function* () {
            var _a;
            (_a = this === null || this === void 0 ? void 0 : this.telemetryHttpServer) === null || _a === void 0 ? void 0 : _a.start();
        });
        this.stop = () => {
            var _a, _b;
            (_a = this.telemetryDataManager) === null || _a === void 0 ? void 0 : _a.disconnect();
            (_b = this === null || this === void 0 ? void 0 : this.telemetryHttpServer) === null || _b === void 0 ? void 0 : _b.stop();
        };
        this.config = deepFreeze(config);
        this.telemetryDataManager = new TelemetryDataManager(config.adapter);
        this.telemetryHttpServer = new TelemetryHttpServer({
            config: config,
            telemetryDataManager: this.telemetryDataManager,
        });
    }
}
//# sourceMappingURL=TelemetryServer.js.map