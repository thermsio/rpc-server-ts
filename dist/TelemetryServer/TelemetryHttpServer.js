var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a;
import Koa from 'koa';
import cors from '@koa/cors';
import logger from 'koa-logger';
import { findDirectoryUpTree } from '../utils/find-dir-up-tree';
import fs from 'fs';
const DEFAULT_HOST = '127.0.0.1';
export class TelemetryHttpServer {
    constructor(options) {
        this.options = options;
        this.start = (onStartedCallback) => {
            var _b;
            if (this.koaAppListener)
                return;
            _a.configureKoaAppRoutes(this.koaApp, this.telemetryDataManager);
            if ('host' in this.options.config.server) {
                const host = ((_b = this.options.config.server) === null || _b === void 0 ? void 0 : _b.host) || DEFAULT_HOST;
                const port = this.options.config.server.port;
                this.koaAppListener = this.koaApp.listen({
                    host,
                    port,
                }, () => {
                    this.log(`Telemetry HTTP server listening ${host}:${port}`);
                    if (onStartedCallback) {
                        onStartedCallback();
                    }
                });
            }
        };
        this.stop = (onStoppedCallback) => {
            var _b;
            (_b = this.koaAppListener) === null || _b === void 0 ? void 0 : _b.close((err) => {
                if (err) {
                    console.error(`telemetry server had an error when stopping`, err);
                }
                else {
                    this.log(`telemetry server stopped`);
                    this.koaAppListener = undefined;
                    if (onStoppedCallback) {
                        onStoppedCallback();
                    }
                }
            });
        };
        this.log = (...args) => {
            console.log(`TelemetryServer `, ...args);
        };
        this.koaApp = new Koa();
        this.koaApp.use(cors());
        this.koaApp.use(logger());
        this.telemetryDataManager = options.telemetryDataManager;
    }
}
_a = TelemetryHttpServer;
TelemetryHttpServer.configureKoaAppRoutes = (app, telemetryDataManager) => {
    app.use((ctx, next) => __awaiter(void 0, void 0, void 0, function* () {
        if (ctx.method !== 'GET')
            return yield next();
        switch (ctx.request.path) {
            case '/':
                // @therms/rpc-telemetry-client is a peer dependency when serving the telemetry client
                const telemetryClientPath = findDirectoryUpTree('node_modules/@therms/rpc-telemetry-client/dist');
                if (!telemetryClientPath) {
                    console.warn('TelemetryHttpServer: @therms/rpc-telemetry-client not found');
                }
                const path = `${telemetryClientPath}/index.html`;
                const telemetryClientHtml = yield fs.promises.readFile(path, 'utf8');
                // stream the html back in the koa response
                ctx.body = telemetryClientHtml;
                ctx.status = 200;
                return;
            case '/docs':
                try {
                    ctx.body = yield telemetryDataManager.getRPCDocs();
                    ctx.status = 200;
                }
                catch (e) {
                    console.log('TelemetryHttpServer error fetching procedure docs:', e);
                    ctx.status = 500;
                }
                return;
            case '/health':
                ctx.body = 'ok';
                ctx.status = 200;
                return;
            case '/heartbeats':
                try {
                    const docs = yield telemetryDataManager.getHeartbeats();
                    ctx.body = docs;
                    ctx.status = 200;
                }
                catch (e) {
                    console.log('TelemetryHttpServer error fetching heartbeats:', e);
                    ctx.status = 500;
                }
                return;
        }
        return yield next();
    }));
};
//# sourceMappingURL=TelemetryHttpServer.js.map