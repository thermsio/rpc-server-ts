var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import mongoose from 'mongoose';
export class MongoManager {
    constructor(mongoURI) {
        this.mongoURI = mongoURI;
        this.showDbConnectionLogs = false;
        this.connected = false;
        this.setupMongoConnection = () => __awaiter(this, void 0, void 0, function* () {
            mongoose.connect(this.mongoURI, {
                autoCreate: true,
            });
            const db = mongoose.connection;
            db.on('disconnected', (info) => {
                if (this.showDbConnectionLogs) {
                    console.log('MongoManager disconnected:', info);
                }
                this.connected = false;
            });
            db.on('error', (err) => {
                console.error('MongoManager connection error:', err);
                this.connected = false;
            });
            db.once('connected', () => {
                if (this.showDbConnectionLogs) {
                    console.log('MongoManager connected:');
                }
                this.connected = true;
            });
        });
        this.tearDownConnection = () => {
            mongoose.disconnect();
        };
        this.showDbConnectionLogs = process.env.NODE_ENV !== 'test';
    }
}
//# sourceMappingURL=MongoManager.js.map