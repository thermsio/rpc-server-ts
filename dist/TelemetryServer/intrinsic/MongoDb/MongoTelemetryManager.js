var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { uniqBy } from 'lodash';
import { HeartbeatModel, RPCDocModel, } from './Models';
import { MongoManager } from './MongoManager';
export class MongoTelemetryManager {
    constructor(adaptorConfig) {
        this.disconnect = () => {
            this.mongoManager.tearDownConnection();
        };
        this.getHeartbeats = () => __awaiter(this, void 0, void 0, function* () {
            return HeartbeatModel.find({}, null, { limit: 500 })
                .lean()
                .exec()
                .then((docs) => {
                return uniqBy(docs, 'ephemeralId');
            });
        });
        this.getProcedureDocs = () => __awaiter(this, void 0, void 0, function* () {
            return RPCDocModel.find({}, null, { limit: 5000 })
                .lean()
                .exec();
        });
        this.saveHeartbeat = (telemetryHeartbeat) => __awaiter(this, void 0, void 0, function* () {
            const doc = new HeartbeatModel(telemetryHeartbeat);
            try {
                yield doc.save();
            }
            catch (err) {
                return console.error('MongoTelemetryManager#saveHeartbeat error:', err);
            }
        });
        this.saveHandlersRegistrationInfo = (procedureDescriptions) => __awaiter(this, void 0, void 0, function* () {
            if (!procedureDescriptions.length)
                return;
            let waitCount = 0;
            while (!this.mongoManager.connected) {
                if (waitCount > 30) {
                    throw Error('MongoManager has not connected to mongoDB');
                }
                waitCount++;
                yield new Promise((r) => setTimeout(r, 100));
            }
            try {
                const serverDisplayName = procedureDescriptions[0].serverDisplayName;
                yield RPCDocModel.deleteMany({ serverDisplayName });
                yield RPCDocModel.insertMany(procedureDescriptions);
            }
            catch (err) {
                console.error('MongoTelemetryManager#saveHandlerRegistrationInfo error:', err);
            }
        });
        this.mongoManager = new MongoManager(adaptorConfig.mongoURI);
        this.mongoManager.setupMongoConnection();
    }
}
//# sourceMappingURL=MongoTelemetryManager.js.map