var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import Koa from 'koa';
import cors from '@koa/cors';
import logger from 'koa-logger';
import koaStatic from 'koa-static';
// @ts-ignore
import koaSendFile from 'koa-sendfile';
import path from 'path';
const DEFAULT_HOST = '127.0.0.1';
export class HttpServer {
    constructor(options) {
        var _a, _b;
        this.options = options;
        this.start = (onStartedCallback) => {
            var _a, _b;
            if (this.koaAppListener)
                return;
            this.configureRoutes();
            const host = ((_a = this.options.config.server) === null || _a === void 0 ? void 0 : _a.host) || DEFAULT_HOST;
            const port = (_b = this.options.config) === null || _b === void 0 ? void 0 : _b.server.port;
            this.koaAppListener = this.koaApp.listen({
                host,
                port,
            }, () => {
                this.log(`Telemetry HTTP server listening ${host}:${port}`);
                if (onStartedCallback) {
                    onStartedCallback();
                }
            });
        };
        this.stop = (onStoppedCallback) => {
            var _a, _b;
            const host = ((_a = this.options.config.server) === null || _a === void 0 ? void 0 : _a.host) || DEFAULT_HOST;
            const port = this.options.config.server.port;
            (_b = this.koaAppListener) === null || _b === void 0 ? void 0 : _b.close((err) => {
                if (err) {
                    // todo: log error
                    // console.log(
                    //   `${this.displayName} had an error when stopping (${host}:${port})`,
                    //   err,
                    // );
                }
                else {
                    this.log(`server stopped (${host}:${port})`);
                    this.koaAppListener = undefined;
                    if (onStoppedCallback) {
                        onStoppedCallback();
                    }
                }
            });
        };
        this.configureRoutes = () => {
            if (this.staticFilesPath) {
                this.koaApp.use(koaStatic(this.staticFilesPath));
            }
            this.koaApp.use((ctx, next) => __awaiter(this, void 0, void 0, function* () {
                switch (ctx.request.path) {
                    case '/docs':
                        try {
                            ctx.body = yield this.telemetryDataManager.getRPCDocs();
                            ctx.status = 200;
                        }
                        catch (e) {
                            console.log('HttpServer error fetching procedure docs:', e);
                            ctx.status = 500;
                        }
                        break;
                    case '/health':
                        ctx.body = 'ok';
                        ctx.status = 200;
                        break;
                    case '/heartbeats':
                        try {
                            const docs = yield this.telemetryDataManager.getHeartbeats();
                            ctx.body = docs;
                            ctx.status = 200;
                        }
                        catch (e) {
                            console.log('HttpServer error fetching heartbeats:', e);
                            ctx.status = 500;
                        }
                        break;
                    default:
                        // default to serve the index.html file if `staticFilesPath` is set
                        if (this.staticFilesPath) {
                            return koaSendFile(ctx, path.join(this.staticFilesPath, 'index.html'));
                        }
                        else {
                            ctx.body = 'Route not recognized and no files found';
                            ctx.status = 404;
                        }
                        break;
                }
                return;
            }));
        };
        this.log = (...args) => {
            console.log(`TelemetryServer `, ...args);
        };
        this.koaApp = new Koa();
        this.koaApp.use(cors());
        this.koaApp.use(logger());
        this.staticFilesPath = (_b = (_a = this.options.config) === null || _a === void 0 ? void 0 : _a.server) === null || _b === void 0 ? void 0 : _b.staticFilesPath;
        this.telemetryDataManager = options.telemetryDataManager;
    }
}
//# sourceMappingURL=HttpServer.js.map