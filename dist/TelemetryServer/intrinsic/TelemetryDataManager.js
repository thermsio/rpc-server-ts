var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { MongoTelemetryManager } from './MongoDb/MongoTelemetryManager';
export class TelemetryDataManager {
    /**
     * If an adaptor is not provided, the TelemetryDataManager will use an in-memory store.
     * @param adaptor
     */
    constructor(adaptor) {
        this.inMemoryRPCDocs = [];
        this.disconnect = () => {
            var _a;
            (_a = this.mongoTelemetryManager) === null || _a === void 0 ? void 0 : _a.disconnect();
        };
        this.getHeartbeats = () => __awaiter(this, void 0, void 0, function* () {
            if (this.mongoTelemetryManager) {
                return this.mongoTelemetryManager.getHeartbeats();
            }
            return [];
        });
        this.getRPCDocs = () => __awaiter(this, void 0, void 0, function* () {
            if (this.mongoTelemetryManager) {
                return this.mongoTelemetryManager.getProcedureDocs();
            }
            return this.inMemoryRPCDocs;
        });
        this.saveHandlersRegistrationInfo = (procedureDescriptions) => __awaiter(this, void 0, void 0, function* () {
            if (!procedureDescriptions.length)
                return;
            if (this.mongoTelemetryManager) {
                yield this.mongoTelemetryManager.saveHandlersRegistrationInfo(procedureDescriptions);
            }
            else {
                this.inMemoryRPCDocs = this.inMemoryRPCDocs.concat(procedureDescriptions);
            }
        });
        this.saveHeartbeat = (heartbeat) => __awaiter(this, void 0, void 0, function* () {
            if (this.mongoTelemetryManager) {
                yield this.mongoTelemetryManager.saveHeartbeat(heartbeat);
            }
        });
        if (adaptor === null || adaptor === void 0 ? void 0 : adaptor.mongoURI) {
            this.mongoTelemetryManager = new MongoTelemetryManager({
                mongoURI: adaptor.mongoURI,
            });
        }
    }
}
//# sourceMappingURL=TelemetryDataManager.js.map