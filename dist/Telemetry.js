export var ETelemetryReportTypes;
(function (ETelemetryReportTypes) {
    ETelemetryReportTypes[ETelemetryReportTypes["Heartbeat"] = 0] = "Heartbeat";
    ETelemetryReportTypes[ETelemetryReportTypes["HandlersRegistration"] = 1] = "HandlersRegistration";
    ETelemetryReportTypes[ETelemetryReportTypes["Statistics"] = 2] = "Statistics";
})(ETelemetryReportTypes || (ETelemetryReportTypes = {}));
//# sourceMappingURL=Telemetry.js.map