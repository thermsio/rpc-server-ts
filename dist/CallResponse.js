import { CallRequest } from './CallRequest';
export class CallResponse {
    /**
     * Check if an object is a valid call response
     * @param obj
     */
    static isCallResponse(obj) {
        if (obj instanceof CallResponse || typeof obj === 'object') {
            return (obj.hasOwnProperty('code') &&
                obj.hasOwnProperty('success') &&
                obj.hasOwnProperty('trace'));
        }
        return false;
    }
    /**
     * Check if an object is a valid call response DTO
     * @param obj
     */
    static isCallResponseDTO(obj) {
        if (typeof obj === 'object') {
            return obj.hasOwnProperty('code') && obj.hasOwnProperty('success');
        }
        return false;
    }
    constructor(response, request) {
        this.code = response.code;
        this.correlationId = request.correlationId || response.correlationId;
        this.data = response.data;
        this.message = response.message;
        this.success = response.success;
        this.trace = {
            id: request.trace.id,
        };
    }
}
CallResponse.EMPTY = new CallResponse({
    code: 500,
    message: 'empty',
    success: false,
}, CallRequest.EMPTY);
CallResponse.toCallResponseDTO = (response) => {
    return {
        code: response.code,
        correlationId: response.correlationId,
        data: response.data,
        message: response.message,
        success: response.success,
    };
};
//# sourceMappingURL=CallResponse.js.map